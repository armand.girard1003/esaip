package com.esaip.projetmob.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.BaseStream;

/**
 * Cette classe regroupe toutes les méthodes utiles à la gestion de la base de données
 * */
public class ContractBdd {
    public ContractBdd(){}

    /**
     * Déclaration de classe abstraite qui servira de modèle pour la table Users de la base de données
     * */
    public static abstract class UsersEntry implements BaseStream {
        public static final String TABLE_NAME = "Users";
        public static final String COLUMN_NAME_Id = "Id";
        public static final String COLUMN_NAME_Email = "Email";
    }

    /**
     * Déclaration de classe abstraite qui servira de modèle pour la table UserData de la base de données
     * */
    public static abstract class UserDataEntry implements BaseStream {
        public static final String TABLE_NAME = "UserDATA";
        public static final String COLUMN_NAME_Id = "DataId";
        public static final String COLUMN_NAME_UserID = "UserId";
        public static final String COLUMN_NAME_Sex = "Sex";
        public static final String COLUMN_NAME_Age = "Age";
        public static final String COLUMN_NAME_Height = "Height";
        public static final String COLUMN_NAME_Weight = "Weight";
        public static final String COLUMN_NAME_Timestamp = "Timestamp";
    }

    /**
     * Création du script de création de la table Users
     * */
    public final static String SQL_CREATE_USERS =
            "CREATE TABLE " + UsersEntry.TABLE_NAME +
                    " (" + UsersEntry.COLUMN_NAME_Id + " TEXT PRIMARY KEY, " +
                    UsersEntry.COLUMN_NAME_Email + " TEXT) ";

    /**
     * Création du script de création de la table UserData
     * */
    public final static String SQL_CREATE_USERDATA =
            "CREATE TABLE " + UserDataEntry.TABLE_NAME +
                    " (" + UserDataEntry.COLUMN_NAME_Id + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    UserDataEntry.COLUMN_NAME_UserID + " INTEGER, " +
                    UserDataEntry.COLUMN_NAME_Sex + " TEXT, " +
                    UserDataEntry.COLUMN_NAME_Age + " TEXT, " +
                    UserDataEntry.COLUMN_NAME_Height + " TEXT, " +
                    UserDataEntry.COLUMN_NAME_Weight + " TEXT, " +
                    UserDataEntry.COLUMN_NAME_Timestamp + " DATETIME DEFAULT CURRENT_TIMESTAMP, " +
                    "FOREIGN KEY(" + UserDataEntry.COLUMN_NAME_UserID + ") REFERENCES " +
                    UsersEntry.TABLE_NAME + "(" + UsersEntry.COLUMN_NAME_Id + "))";

    /**
     * Création du script de suppression de la table Users
     * */
    public static final String SQL_DELETE_USERS =
            "DROP TABLE IF EXISTS " + UsersEntry.TABLE_NAME;

    /**
     * Création du script de suppression de la table UserData
     * */
    public static final String SQL_DELETE_USERDATA =
            "DROP TABLE IF EXISTS " + UsersEntry.TABLE_NAME;

    /**
     * Cette méthode insert un nouvelle utilisateur dans la table Users de la base de données
     *
     * @param email Email de l'utilisateur
     * @param userId Id de l'utilisateur (le même que l'uid présent dans Firebase)
     * @param context Context d'appel de la fonction
     * */
    public static void insertUser(String email, String userId, Context context) {
        // Déclaration de BddHelper pour accéder à la base de données en mode écriture
        BddHelper dbHelper = new BddHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        // Configuration des valeurs à insérer, ici l'email et le User Id
        ContentValues values = new ContentValues();
        values.put(UsersEntry.COLUMN_NAME_Email, email);
        values.put(UsersEntry.COLUMN_NAME_Id, userId);

        // Insertion dans la table Users
        db.insert(UsersEntry.TABLE_NAME, null, values);
    }

    /**
     * Cette méthode insert de nouvelles données associées à un utilisateur dans la table UserData de la base de données
     *
     * @param userId Id de l'utilisateur pour identifier le propriétaire des données
     * @param sex Sexe que l'utilisateur a sélectionné
     * @param age Age que l'utilisateur a sélectionné en donnant sa date de naissance
     * @param height Taille que l'utilisateur a sélectionné
     * @param weight Poids que l'utilisateur a sélectionné
     * @param context Context d'appel de la fonction
     * */
    public static void insertUserData(String userId, String sex, String age, String height, String weight, Context context) {
        // Déclaration de BddHelper pour accéder à la base de données en mode écriture
        BddHelper dbHelper = new BddHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        // Configuration des valeurs à insérer, ici le User Id, le sexe, l'âge, la taille et le poids
        ContentValues values = new ContentValues();
        values.put(UserDataEntry.COLUMN_NAME_UserID, userId);
        values.put(UserDataEntry.COLUMN_NAME_Sex, sex);
        values.put(UserDataEntry.COLUMN_NAME_Age, age);
        values.put(UserDataEntry.COLUMN_NAME_Height, height);
        values.put(UserDataEntry.COLUMN_NAME_Weight, weight);

        // Insertion dans la table UserData
        db.insert(UserDataEntry.TABLE_NAME, null, values);
    }

    /**
     * Cette méthode récupère dans la base de données le User Id associé à l'email donnée
     *
     * @param email Email de l'utilisateur
     * @param context Context d'appel de la fonction
     * @return User Id associé à l'email
     * */
    public static String getUserIdByEmail(String email, Context context) {
        // Déclaration de BddHelper pour accéder à la base de données en mode lecture
        BddHelper dbHelper = new BddHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String userId = "-1";

        // Définir la requête SQL
        String query =
                "SELECT " + UsersEntry.COLUMN_NAME_Id +
                        " FROM " + UsersEntry.TABLE_NAME +
                        " WHERE " + UsersEntry.COLUMN_NAME_Email + " = ?";

        Cursor cursor = null;

        try {
            // Exécution de la requête avec l'email en paramètre
            cursor = db.rawQuery(query, new String[]{email});

            // Vérifier si des résultats ont été trouvés
            if (cursor.moveToFirst()) userId = cursor.getString(cursor.getColumnIndexOrThrow(UsersEntry.COLUMN_NAME_Id));
        }
        catch (Exception e) {
            Log.e("getUserIdByEmail", "Error accession the database : " + e.getMessage());
        }
        finally {
            if (cursor != null) cursor.close();
            if (db != null) db.close();
        }

        return userId;
    }

    /**
     * Cette méthode vérifie la présence de données, conforment à l'email donnée, dans la table Users
     *
     * @param email Email de l'utilisateur
     * @param context Context d'appel de la fonction
     * @return True si des données sont présentes, sinon False
     * */
    public static boolean checkIfExist(String email, Context context) {
        // Déclaration de BddHelper pour accéder à la base de données en mode lecture
        BddHelper dbHelper = new BddHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        boolean exists = false;

        // Définir la requête SQL
        String query = "SELECT 1 FROM " + UsersEntry.TABLE_NAME +
                " WHERE " + UsersEntry.COLUMN_NAME_Email + " = ? LIMIT 1";

        Cursor cursor = null;

        try {
            // Exécution de la requête avec l'email en paramètre
            cursor = db.rawQuery(query, new String[]{email});

            // Vérifier si un résultat a été trouvé
            if (cursor.moveToFirst()) exists = true;
        }
        catch (Exception e) {
            Log.e("checkIfExist", "Error accessing the database : " + e.getMessage());
        }
        finally {
            if (cursor != null) cursor.close();
            if (db != null) db.close();
        }

        return exists;
    }

    /**
     * Cette méthode vérifie la présence de données, conforment à l'Id de l'utilisateur donné, dans la table UserData
     *
     * @param uid Id de l'utilisateur
     * @param context Context d'appel de la fonction
     * @return True si des données sont présentes, sinon False
     * */
    public static boolean checkIfData(String uid, Context context) {
        // Déclaration de BddHelper pour accéder à la base de données en mode lecture
        BddHelper dbHelper = new BddHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        boolean exists = false;

        // Définir la requête SQL
        String query = "SELECT 1 FROM " + UserDataEntry.TABLE_NAME +
                " WHERE " + UserDataEntry.COLUMN_NAME_UserID + " = ? LIMIT 1";

        Cursor cursor = null;

        try {
            // Exécution de la requête
            cursor = db.rawQuery(query, new String[]{uid});

            // Vérifier si un résultat a été trouvé
            if (cursor.moveToFirst()) exists = true;
        }
        catch (Exception e) {
            Log.e("checkIfData", "Error accessing the database : " + e.getMessage());
        }
        finally {
            if (cursor != null) cursor.close();
            if (db != null) db.close();
        }

        return exists;
    }

    /**
     * Cette méthode récupère les données les plus récentes associées au User Id.
     * Elle prend les données associées, les tries par ordre décroissant, et prend les 1ere.
     *
     * @param uid Id de l'utilisateur
     * @param context Context d'appel de la fonction
     * @return Un tableau a 1 dimension avec 4 données de type String, chacune contenant respectivement sex, age, height, weight
     * */
    public static String [] getCurrentdata(String uid, Context context) {
        // Déclaration de BddHelper pour accéder à la base de données en mode lecture
        BddHelper dbHelper = new BddHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        // Déclaration du tableau qui sera retourné
        String[] result = new String[4];

        // Définir la requête SQL
        String query = "SELECT " +
                UserDataEntry.COLUMN_NAME_Sex + ", " +
                UserDataEntry.COLUMN_NAME_Age + ", " +
                UserDataEntry.COLUMN_NAME_Height + ", " +
                UserDataEntry.COLUMN_NAME_Weight +
                " FROM " + UserDataEntry.TABLE_NAME +
                " WHERE UserId = ? ORDER BY DataId DESC " +
                "LIMIT 1";

        Cursor cursor = null;

        try {
            // Exécution de la requête
            cursor = db.rawQuery(query, new String[]{uid});

            // Récupération des données
            if (cursor.moveToFirst()) {
                if (cursor.getString(cursor.getColumnIndexOrThrow(UserDataEntry.COLUMN_NAME_Sex)) != null) {
                    result[0] = cursor.getString(cursor.getColumnIndexOrThrow(UserDataEntry.COLUMN_NAME_Sex));
                }
                if (cursor.getString(cursor.getColumnIndexOrThrow(UserDataEntry.COLUMN_NAME_Age)) != null) {
                    result[1] = cursor.getString(cursor.getColumnIndexOrThrow(UserDataEntry.COLUMN_NAME_Age));
                }
                if (cursor.getString(cursor.getColumnIndexOrThrow(UserDataEntry.COLUMN_NAME_Height)) != null) {
                    result[2] = cursor.getString(cursor.getColumnIndexOrThrow(UserDataEntry.COLUMN_NAME_Height));
                }
                if (cursor.getString(cursor.getColumnIndexOrThrow(UserDataEntry.COLUMN_NAME_Weight)) != null) {
                    result[3] = cursor.getString(cursor.getColumnIndexOrThrow(UserDataEntry.COLUMN_NAME_Weight));
                }
            }
        }
        catch (Exception e) {
            Log.e("getCurrentdata", "Error accessing the database : " + e.getMessage());
        }
        finally {
            if (cursor != null) cursor.close();
            if (db != null) db.close();
        }

        return result;
    }

    /**
     * Cette méthode récupère un nombre données voulu de l'utilisateur associées à son Id
     *
     * @param uid Id de l'utilisateur
     * @param nb Nombre de données voulues
     * @return Une liste des données récupérées
     * */
    public static List<String[]> getUserData(String uid, int nb, Context context) {
        // Déclartion de BddHelper pour accéder à la base de données en mode lecture
        BddHelper dbHelper = new BddHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        List<String[]> result = new ArrayList<>();

        // Définir la requête SQL
        String query = "SELECT " +
                UserDataEntry.COLUMN_NAME_Sex + ", " +
                UserDataEntry.COLUMN_NAME_Age + ", " +
                UserDataEntry.COLUMN_NAME_Height + ", " +
                UserDataEntry.COLUMN_NAME_Weight + ", " +
                UserDataEntry.COLUMN_NAME_Timestamp +
                " FROM " + UserDataEntry.TABLE_NAME +
                " WHERE UserId = ? ORDER BY DataId DESC " +
                "LIMIT " + nb;

        Cursor cursor = null;

        try {
            // Exécution de la requête
            cursor = db.rawQuery(query, new String[]{uid});

            // Récupération des données
            if (cursor.moveToFirst()) {
                do {
                    String[] row = new String[5];
                    if (cursor.getString(cursor.getColumnIndexOrThrow(UserDataEntry.COLUMN_NAME_Sex)) != null) {
                        row[0] = cursor.getString(cursor.getColumnIndexOrThrow(UserDataEntry.COLUMN_NAME_Sex));
                    }
                    if (cursor.getString(cursor.getColumnIndexOrThrow(UserDataEntry.COLUMN_NAME_Age)) != null) {
                        row[1] = cursor.getString(cursor.getColumnIndexOrThrow(UserDataEntry.COLUMN_NAME_Age));
                    }
                    if (cursor.getString(cursor.getColumnIndexOrThrow(UserDataEntry.COLUMN_NAME_Height)) != null) {
                        row[2] = cursor.getString(cursor.getColumnIndexOrThrow(UserDataEntry.COLUMN_NAME_Height));
                    }
                    if (cursor.getString(cursor.getColumnIndexOrThrow(UserDataEntry.COLUMN_NAME_Weight)) != null) {
                        row[3] = cursor.getString(cursor.getColumnIndexOrThrow(UserDataEntry.COLUMN_NAME_Weight));
                    }
                    if (cursor.getString(cursor.getColumnIndexOrThrow(UserDataEntry.COLUMN_NAME_Timestamp)) != null) {
                        row[4] = cursor.getString(cursor.getColumnIndexOrThrow(UserDataEntry.COLUMN_NAME_Timestamp));
                    }
                    result.add(row);
                } while (cursor.moveToNext());
            } else Log.e("getUserData", "Cursor is empty.");
        }
        catch (Exception e) {
            Log.e("getUserData", "Error accessing the database : " + e.getMessage());
        }
        finally {
            if (cursor != null) cursor.close();
            if (db != null) db.close();
        }

        return result;
    }

    //region Get UserData 1 per 1
    /**
     * Cette méthode récupère la dernière données de l'utilisateur de la colonne Sex
     *
     * @param uid User Id
     * @param context Contexte d'appel de la fonction
     * @return Sexe enregistré de l'utilisiateur
     * */
    public static String getUserLastSex(String uid, Context context) {
        // Déclaration de BddHelper pour accéder à la base de données en mode lecture
        BddHelper dbHelper = new BddHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String sex = null;

        // Définir la requête SQL
        String query = "SELECT " + UserDataEntry.COLUMN_NAME_Sex +
                " FROM " + UserDataEntry.TABLE_NAME +
                " WHERE UserId = ? ORDER BY DataId DESC LIMIT 1";

        Cursor cursor = null;

        try {
            // Exécution de la requête
            cursor = db.rawQuery(query, new String[]{uid});

            // Récupération de la donnée
            if (cursor.moveToFirst()) sex = cursor.getString(cursor.getColumnIndexOrThrow(UserDataEntry.COLUMN_NAME_Sex));
        }
        catch (Exception e) {
            Log.e("getUserSex", "Error accessing the database : " + e.getMessage());
        }
        finally {
            if (cursor != null) cursor.close();
            if (db != null) db.close();
        }

        return sex;
    }

    /**
     * Cette méthode récupère la dernière données de l'utilisateur de la colonne Age
     *
     * @param uid User Id
     * @param context Contexte d'appel de la fonction
     * @return Age enregistré de l'utilisiateur
     * */
    public static String getUserLastAge(String uid, Context context) {
        // Déclaration de BddHelper pour accéder à la base de données en mode lecture
        BddHelper dbHelper = new BddHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String age = null;

        // Définir la requête SQL
        String query = "SELECT " + UserDataEntry.COLUMN_NAME_Age +
                " FROM " + UserDataEntry.TABLE_NAME +
                " WHERE UserId = ? ORDER BY DataId DESC LIMIT 1";

        Cursor cursor = null;

        try {
            // Exécution de la requête
            cursor = db.rawQuery(query, new String[]{uid});

            // Récupération de la donnée
            if (cursor.moveToFirst()) age = cursor.getString(cursor.getColumnIndexOrThrow(UserDataEntry.COLUMN_NAME_Age));
        }
        catch (Exception e) {
            Log.e("getUserAge", "Error accessing the database : " + e.getMessage());
        }
        finally {
            if (cursor != null) cursor.close();
            if (db != null) db.close();
        }

        return age;
    }

    /**
     * Cette méthode récupère la dernière données de l'utilisateur de la colonne Height
     *
     * @param uid User Id
     * @param context Contexte d'appel de la fonction
     * @return Taille enregistré de l'utilisiateur
     * */
    public static String getUserLastHeight(String uid, Context context) {
        // Déclaration de BddHelper pour accéder à la base de données en mode lecture
        BddHelper dbHelper = new BddHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String height = null;

        // Définir la requête SQL
        String query = "SELECT " + UserDataEntry.COLUMN_NAME_Height +
                " FROM " + UserDataEntry.TABLE_NAME +
                " WHERE UserId = ? ORDER BY DataId DESC LIMIT 1";

        Cursor cursor = null;

        try {
            // Exécution de la requête
            cursor = db.rawQuery(query, new String[]{uid});

            // Récupération de la donnée
            if (cursor.moveToFirst()) height = cursor.getString(cursor.getColumnIndexOrThrow(UserDataEntry.COLUMN_NAME_Height));
        }
        catch (Exception e) {
            Log.e("getUserHeight", "Error accessing the database : " + e.getMessage());
        }
        finally {
            if (cursor != null) cursor.close();
            if (db != null) db.close();
        }

        return height;
    }

    /**
     * Cette méthode récupère la dernière données de l'utilisateur de la colonne Weight
     *
     * @param uid User Id
     * @param context Contexte d'appel de la fonction
     * @return Poids enregistré de l'utilisiateur
     * */
    public static String getUserLastWeight(String uid, Context context) {
        // Déclaration de BddHelper pour accéder à la base de données en mode lecture
        BddHelper dbHelper = new BddHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String weight = null;

        // Définir la requête SQL
        String query = "SELECT " + UserDataEntry.COLUMN_NAME_Weight +
                " FROM " + UserDataEntry.TABLE_NAME +
                " WHERE UserId = ? ORDER BY DataId DESC LIMIT 1";

        Cursor cursor = null;

        try {
            // Exécution de la requête
            cursor = db.rawQuery(query, new String[]{uid});

            // Récupération de la donnée
            if (cursor.moveToFirst()) weight = cursor.getString(cursor.getColumnIndexOrThrow(UserDataEntry.COLUMN_NAME_Weight));
        }
        catch (Exception e) {
            Log.e("getUserWeight", "Error accessing the database : " + e.getMessage());
        }
        finally {
            if (cursor != null) cursor.close();
            if (db != null) db.close();
        }

        return weight;
    }
    //endregion
}
