package com.esaip.projetmob.main;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.esaip.projetmob.R;
import com.esaip.projetmob.database.ContractBdd;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class ChartFragment extends Fragment {

    private FirebaseAuth mAuth;
    private FirebaseUser user;
    private String uid = null;

    private List<String> xValues;

    private LineChart chart;

    private View view;

    private List<String[]> data = new ArrayList<>();

    public ChartFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_chart, container, false);

        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();

        chart = view.findViewById(R.id.lineChart);
        if (user != null) {
            uid = user.getUid();

            data = ContractBdd.getUserData(uid, 3, view.getContext());
            if (!data.isEmpty()) {
                Collections.reverse(data);
                updateChartData(data);
            }
            else {
                Toast.makeText(view.getContext(), "No data", Toast.LENGTH_SHORT).show();
            }
        }

        // Inflate the layout for this fragment
        return view;
    }

    private void updateChartData(List<String[]> data) {
        Description description = new Description();
        description.setText("Height/Weight progress");
        description.setPosition(300f,50f);
        chart.setDescription(description);
        chart.getAxisRight().setDrawLabels(true);

        ArrayList<Entry> heightEntries = new ArrayList<>();
        ArrayList<Entry> weightEntries = new ArrayList<>();
        ArrayList<String> timeLabels = new ArrayList<>();

        for (int i = 0 ; i < data.size() ; i++) {
            float height = stringToFloat(data.get(i)[2]);
            float weight = stringToFloat(data.get(i)[3]);
            String time = formatDate(data.get(i)[4]);

            heightEntries.add(new Entry(i, height));
            weightEntries.add(new Entry(i, weight));
            timeLabels.add(time);
        }

        if (!heightEntries.isEmpty() || !weightEntries.isEmpty() || !timeLabels.isEmpty()) {
            LineDataSet heightDataSet = new LineDataSet(heightEntries, "Taille");
            heightDataSet.setAxisDependency(YAxis.AxisDependency.LEFT);
            heightDataSet.setColor(ColorTemplate.getHoloBlue());

            LineDataSet weightDataSet = new LineDataSet(weightEntries, "Poids");
            weightDataSet.setAxisDependency(YAxis.AxisDependency.RIGHT);
            weightDataSet.setColor(Color.RED);

            LineData lineData = new LineData(heightDataSet, weightDataSet);
            chart.setData(lineData);

            XAxis xAxis = chart.getXAxis();
            xAxis.setEnabled(true);
            xAxis.setValueFormatter(new IndexAxisValueFormatter(timeLabels));
            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

            xAxis.setLabelCount(4);
            xAxis.setGranularity(1f);

            YAxis leftAxis = chart.getAxisLeft();
            YAxis rightAxis = chart.getAxisRight();
            rightAxis.setDrawLabels(true);
            leftAxis.setDrawLabels(true);

            chart.invalidate();
        }

    }

    public float stringToFloat(String value) {
        String splitString = value.split(" ")[0];
        return Float.parseFloat(splitString);
    }

    public String formatDate(String dateTime) {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        @SuppressLint("SimpleDateFormat") SimpleDateFormat outputFormat = new SimpleDateFormat("MMM dd yy");
        String formattedDate = "";

        try {
            Date date = inputFormat.parse(dateTime);
            if (date != null) {
                formattedDate = outputFormat.format(date);
            }
        } catch (ParseException e) {
            Log.e("ChartFragment", "Error formatting date : " + e.getMessage());
        }

        return formattedDate;
    }
}