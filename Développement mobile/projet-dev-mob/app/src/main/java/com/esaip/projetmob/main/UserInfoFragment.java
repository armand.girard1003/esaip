package com.esaip.projetmob.main;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.esaip.projetmob.R;

public class UserInfoFragment extends Fragment {

    private EditText editTextWeight, editTextHeight, editTextBirthDate;
    private RadioGroup radioGroupGender;
    private Spinner goalSpinner;

    private String[] goals = {"Perdre du poids", "Prendre du poids", "Maintenir son poids"};

    public UserInfoFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_info, container, false);

        editTextWeight = view.findViewById(R.id.editTextWeight);
        editTextHeight = view.findViewById(R.id.editTextHeight);
        editTextBirthDate = view.findViewById(R.id.editTextBirthDate);
        radioGroupGender = view.findViewById(R.id.radioGroupGender);
        goalSpinner = view.findViewById(R.id.goalSpinner);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, goals);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        goalSpinner.setAdapter(adapter);

        return view;
    }
}
