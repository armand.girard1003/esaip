package com.esaip.projetmob.connection;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentContainerView;
import androidx.fragment.app.FragmentTransaction;

import com.esaip.projetmob.main.MainActivity;
import com.esaip.projetmob.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class Connection extends AppCompatActivity {

    private FragmentContainerView fragContainer;

    private LoginFragment loginFragment;

    private Button change;

    private boolean state = true; // True == Login & False == Create

    // Champs de texte pour entrer l'email et le mot de passe
    private EditText editTextMail, editTextPwd;

    // Instance FirebaseAuth pour gérer l'authentification
    private static FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connection);

        fragContainer = findViewById(R.id.fragContainer);

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragContainer, new LoginFragment()).commit();

        change = findViewById(R.id.changeConnection);
        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeButton();
            }
        });

        mAuth = FirebaseAuth.getInstance();
        loginFragment = (LoginFragment) getSupportFragmentManager().findFragmentById(R.id.fragContainer);

    }

    public void changeButton() {
        if (state) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragContainer, new CreateFragment()).commit();
            if (change == null) {
                Toast.makeText(Connection.this, "change est null", Toast.LENGTH_SHORT).show();
            }
            else {
                change.setText("Login to your account");
            }
            state = false;
        }
        else {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragContainer, new LoginFragment()).commit();
            change.setText("Create your account");
            state = true;
        }
    }

    @Override
    public void onStart(){
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null) {
            // Redirige l'utilisateur connecté vers l'activité FirstConnection
            Intent i = new Intent(Connection.this, MainActivity.class);
            startActivity(i);
            finish();
        }
    }
}