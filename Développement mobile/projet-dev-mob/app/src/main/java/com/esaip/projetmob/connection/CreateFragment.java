package com.esaip.projetmob.connection;



import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.esaip.projetmob.R;
import com.esaip.projetmob.database.ContractBdd;
import com.esaip.projetmob.main.MainActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Calendar;

public class CreateFragment extends Fragment {

    private EditText editTextMail, editTextPwd, editTextHeight, editTextWeight, editTextBirthDate;
    private RadioGroup radioGroupGender;
    private Spinner goalSpinner;
    private static FirebaseAuth mAuth;
    private View view;

    public CreateFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_create, container, false);
        mAuth = FirebaseAuth.getInstance();

        editTextMail = view.findViewById(R.id.mailFragmentCreate);
        editTextPwd = view.findViewById(R.id.pwdFragmentCreate);
        editTextHeight = view.findViewById(R.id.editTextHeight);
        editTextWeight = view.findViewById(R.id.editTextWeight);
        editTextBirthDate = view.findViewById(R.id.editTextBirthDate);
        radioGroupGender = view.findViewById(R.id.radioGroupGender);
        goalSpinner = view.findViewById(R.id.goalSpinner);

        editTextBirthDate.setOnClickListener(v -> showDatePicker());

        view.findViewById(R.id.RegisterFragmentCreate).setOnClickListener(v -> buttonRegister());

        return view;
    }

    private void showDatePicker() {
        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), (view, year1, month1, dayOfMonth) -> {
            String date = dayOfMonth + "/" + (month1 + 1) + "/" + year1;
            editTextBirthDate.setText(date);
        }, year, month, day);

        datePickerDialog.show();
    }

    private void buttonRegister() {
        String email = editTextMail.getText().toString().trim();
        String password = editTextPwd.getText().toString().trim();
        String height = editTextHeight.getText().toString().trim();
        String weight = editTextWeight.getText().toString().trim();
        String birthDate = editTextBirthDate.getText().toString().trim();
        int selectedGenderId = radioGroupGender.getCheckedRadioButtonId();
        RadioButton selectedGender = view.findViewById(selectedGenderId);
        String gender = (selectedGender != null) ? selectedGender.getText().toString() : "";
        String goal = goalSpinner.getSelectedItem().toString();

        if (TextUtils.isEmpty(email) || TextUtils.isEmpty(password) || TextUtils.isEmpty(height) ||
                TextUtils.isEmpty(weight) || TextUtils.isEmpty(birthDate) || TextUtils.isEmpty(gender) || goal.isEmpty()) {
            Toast.makeText(getContext(), "Veuillez remplir tous les champs", Toast.LENGTH_SHORT).show();
            return;
        }

        if (password.length() < 6) {
            editTextPwd.setError("Le mot de passe doit contenir au moins 6 caractères.");
            return;
        }

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d("Register", "createUserWithEmail:success");
                            ContractBdd.insertUser(email, task.getResult().getUser().getUid(), view.getContext());
                            Intent i = new Intent(view.getContext(), MainActivity.class);
                            startActivity(i);
                            getActivity().finish();

                            // Redirection vers UserInfoFragment après inscription
                            getFragmentManager().beginTransaction()
                                    .replace(R.id.fragment_container, new UserInfoFragment()) // Remplace le fragment actuel par UserInfoFragment
                                    .addToBackStack(null) // Ajoute la transaction à la pile arrière
                                    .commit();


                        } else {
                            Log.w("Register", "createUserWithEmail:failure", task.getException());
                            Toast.makeText(view.getContext(), "Failed to register.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    @Override
    public void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null) {
            Intent i = new Intent(view.getContext(), MainActivity.class);
            startActivity(i);
            getActivity().finish();
        }
    }

    private class UserInfoFragment extends Fragment {
    }
}
