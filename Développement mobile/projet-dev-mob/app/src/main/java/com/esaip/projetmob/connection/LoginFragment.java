package com.esaip.projetmob.connection;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.esaip.projetmob.main.MainActivity;
import com.esaip.projetmob.R;
import com.esaip.projetmob.database.ContractBdd;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoginFragment extends Fragment {

    // Champs de texte pour entrer l'email et le mot de passe
    private EditText editTextMail, editTextPwd;

    // Instance FirebaseAuth pour gérer l'authentification
    private static FirebaseAuth mAuth;

    private View view;

    private String uid;

    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mAuth = FirebaseAuth.getInstance();

        view = inflater.inflate(R.layout.fragment_login, container, false);

        editTextMail = view.findViewById(R.id.mailFragmentLogin);
        editTextPwd = view.findViewById(R.id.pwdFragmentLogin);

        view.findViewById(R.id.LoginFragmentLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buttonLogin();
            }
        });

        // Inflate the layout for this fragment
        return view;
    }

    public void buttonLogin() {
        String email = editTextMail.getText().toString();
        String password = editTextPwd.getText().toString();

        if (email == null || TextUtils.isEmpty(email)) {
            editTextMail.setError("Please enter an email.");
        }
        else if (password == null || TextUtils.isEmpty(password)) {
            editTextPwd.setError("Please enter a password.");
        }
        else {
            mAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                Log.d("Login", "connectUserWithEmail:success");
                                uid = task.getResult().getUser().getUid();
                                if (ContractBdd.checkIfExist(email, view.getContext())) {
                                    if (ContractBdd.checkIfData(uid, view.getContext())) {
                                        Intent i = new Intent(view.getContext(), MainActivity.class);
                                        startActivity(i);
                                        getActivity().finish();
                                    }
                                    else {
                                        Intent i = new Intent(view.getContext(), MainActivity.class);
                                        startActivity(i);
                                        getActivity().finish();
                                    }

                                    // Redirection vers WeightHeightFragment après connexion
                                getFragmentManager().beginTransaction()
                                        .replace(R.id.fragment_container, new WeightHeightFragment()) // Remplace le fragment actuel par WeightHeightFragment
                                        .addToBackStack(null) // Ajoute la transaction à la pile arrière
                                        .commit();
                                }
                                else {
                                    ContractBdd.insertUser(email, uid, view.getContext());
                                    Intent i = new Intent(view.getContext(), MainActivity.class);
                                    startActivity(i);
                                    getActivity().finish();
                                }
                            }
                            else {
                                Log.w("Login", "createUserWithEmail:failure", task.getException());
                                Toast.makeText(view.getContext(), "Failed to connect.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null) {
            // Redirige l'utilisateur connecté vers l'activité FirstConnection
            Intent i = new Intent(view.getContext(), MainActivity.class);
            startActivity(i);
            getActivity().finish();
        }
    }

    private class WeightHeightFragment extends Fragment {
    }
}