package com.esaip.projetmob.main;

import android.util.Log;

import org.json.JSONObject;

import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

public class OpenAi {

    private static final String APIKey = "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";
    private static final String API_URL = "https://api.openai.com/v1/completions";

    public static void envoyerRequete(String prompt, Callback callback) {
        OkHttpClient client = new OkHttpClient();

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("model", "textèxt-davinci-003");
            jsonObject.put("prompt", prompt);
            jsonObject.put("max_tokens", 4000);
        }
        catch (Exception e) {
            Log.e("OpenAI", "Erreur lors de l'envoie de la requête : " + e.getMessage());
        }

        RequestBody body = RequestBody.create(
                jsonObject.toString(),
                MediaType.parse("application/json; charset=utf-8")
        );

        Request request = new Request.Builder()
                .url(API_URL)
                .header("Authorization", "Bearer " + APIKey)
                .post(body)
                .build();

        client.newCall(request).enqueue(callback);
    }

}
