package com.esaip.projetmob.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.esaip.projetmob.R;
import com.esaip.projetmob.connection.Connection;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();
        if (user != null) {
            String email = user.getEmail();
            TextView userEmail = findViewById(R.id.userEmail);
        }
        else {
            Toast.makeText(this, "No user", Toast.LENGTH_SHORT).show();
        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawerLayout = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open_nav, R.string.close_nav);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        if (savedInstanceState == null) {
            Intent intent = getIntent();
            String target = intent.getStringExtra("target");
            if (target != null) {
                String navHomeId = String.valueOf(R.id.nav_home);
                String navAddDataId = String.valueOf(R.id.nav_addData);
                String navViewDataId = String.valueOf(R.id.nav_viewData);
                String navMealId = String.valueOf(R.id.nav_meal);
                String navSettingsId = String.valueOf(R.id.nav_settings);
                String navShareId = String.valueOf(R.id.nav_share);
                String navAboutUsId = String.valueOf(R.id.nav_aboutus);

                if (target.equals(navHomeId)) {
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment()).commit();
                    navigationView.setCheckedItem(R.id.nav_home);
                }
                else if (target.equals(navAddDataId)) {
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new AddDataFragment()).commit();
                    navigationView.setCheckedItem(R.id.nav_addData);
                }
                else if (target.equals(navViewDataId)) {
                    Intent i = new Intent(MainActivity.this, Data.class);
                    startActivity(i);
                    finish();
                }
                else if (target.equals(navMealId)) {
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new MealFragment()).commit();
                    navigationView.setCheckedItem(R.id.nav_meal);
                }
                else if (target.equals(navSettingsId)) {
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new SettingsFragment()).commit();
                    navigationView.setCheckedItem(R.id.nav_settings);
                }
                else if (target.equals(navShareId)) {
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ShareFragment()).commit();
                    navigationView.setCheckedItem(R.id.nav_share);
                }
                else if (target.equals(navAboutUsId)) {
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new AboutFragment()).commit();
                    navigationView.setCheckedItem(R.id.nav_aboutus);
                }
                else {
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment()).commit();
                    navigationView.setCheckedItem(R.id.nav_home);
                }
            }
            else {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment()).commit();
                navigationView.setCheckedItem(R.id.nav_home);
            }
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.nav_home) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment()).commit();
        }
        else if (item.getItemId() == R.id.nav_addData) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new AddDataFragment()).commit();
        }
        else if (item.getItemId() == R.id.nav_viewData) {
            Intent i = new Intent(MainActivity.this, Data.class);
            startActivity(i);
            finish();
        }
        else if (item.getItemId() == R.id.nav_settings) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new SettingsFragment()).commit();
        }
        else if (item.getItemId() == R.id.nav_share) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ShareFragment()).commit();
        }
        else if (item.getItemId() == R.id.nav_aboutus) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new AboutFragment()).commit();
        }
        else if (item.getItemId() == R.id.nav_logout) {
            Toast.makeText(this, "Logout", Toast.LENGTH_SHORT).show();
            FirebaseAuth.getInstance().signOut();
            Intent i = new Intent(this, Connection.class);
            startActivity(i);
            finish();
        }

        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        }
        else {
            super.onBackPressed();
        }
    }
}