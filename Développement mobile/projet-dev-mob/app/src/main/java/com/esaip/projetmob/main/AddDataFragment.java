package com.esaip.projetmob.main;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.esaip.projetmob.R;
import com.esaip.projetmob.database.ContractBdd;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class AddDataFragment extends Fragment {

    private View view;

    private FirebaseAuth auth;
    private FirebaseUser user;
    private Button register, dateButton;
    private TextView heightText, weightText;
    private RadioGroup sex;
    private RadioButton male, female;

    private DatePickerDialog datePickerDialog;
    private SeekBar heightBar, weightBar;
    private int minWeight = 50, maxWeight = 100, minHeight = 100, maxHeight = 200;

    private String[] currentData = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_add_data, container, false);

        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();

        initialise();

        String userId = user.getUid();

        if (ContractBdd.checkIfData(userId, view.getContext())) {
            currentData = new String[4];
            currentData = ContractBdd.getCurrentdata(userId, view.getContext());
            setCurrentData();
        }
        else setDefaultData();

        // Ajouter un écouteur pour enregistrer les données
        register = view.findViewById(R.id.saveAddData);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveUserData(userId);
            }
        });

        dateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDatePicker(view);
            }
        });

        // Inflate the layout for this fragment
        return view;
    }

    private void openDatePicker(View view) {
        if (datePickerDialog == null)
            initDatePicker(null, null, null);
        datePickerDialog.show();
    }

    private void saveUserData(String userId) {
        String sexChoice = "";
        int selectedId = sex.getCheckedRadioButtonId();
        if (selectedId == male.getId())         sexChoice = "Male";
        else if (selectedId == female.getId())  sexChoice = "Female";
        else {
            Toast.makeText(view.getContext(), "Select a sex", Toast.LENGTH_SHORT).show();
            return;
        }

        if (dateButton.getText().toString().equals(getTodayDate())) {
            Toast.makeText(view.getContext(), "Select a birthday", Toast.LENGTH_SHORT).show();
            return;
        }

        // Récupérer et insérer les valeurs entrées par l'utilisateur
        ContractBdd.insertUserData(
                userId,
                sexChoice,
                dateButton.getText().toString(),
                heightText.getText().toString(),
                weightText.getText().toString(),
                view.getContext()
        );

        Toast.makeText(view.getContext(), "User data saved", Toast.LENGTH_SHORT).show();
    }

    private void setDefaultData() {
        // Date
        initDatePicker(null, null, null);

        // Height
        if (currentData != null) heightText.setText(minHeight + " cm");

        // Weight
        if (currentData == null) weightText.setText(minWeight + " kg");
    }

    private void setCurrentData() {
        // Sex
        if (currentData[0] != null && !currentData[0].isEmpty()) {
            if (currentData[0].equals("Male")) male.setChecked(true);
            else female.setChecked(true);
        }

        // Date
        if (!currentData[1].isEmpty()) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd yyyy", Locale.ENGLISH);

            try {
                Date date = dateFormat.parse(currentData[1]);

                // Extraction
                SimpleDateFormat dayFormat = new SimpleDateFormat("dd");
                SimpleDateFormat monthFormat = new SimpleDateFormat("MMM");
                SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");

                String day = dayFormat.format(date);
                String month = monthFormat.format(date);
                String year = yearFormat.format(date);

                // Initialisation
                initDatePicker(day, month, year);

                dateButton.setText(currentData[1]);
            } catch (ParseException e) {
                Log.e("DateParser", "Date parsing error : " + e.getMessage());
            }
        }

        // Height
        if (!currentData[2].isEmpty()) {
            heightText.setText(currentData[2] + " cm");
            String[] heightString = currentData[2].split(" ");
            heightBar.setProgress(Integer.parseInt(heightString[0]) - minHeight);
        }

        // Weight
        if (!currentData[3].isEmpty()) {
            weightText.setText(currentData[3] + " kg");
            String[] weightString = currentData[3].split(" ");
            weightBar.setProgress(Integer.parseInt(weightString[0]) - minWeight);
        }
    }

    public void initDatePicker(String day, String month, String year) {
        DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                String date = makeDateString(day, month, year);
                dateButton.setText(date);
            }
        };
        Calendar cal = Calendar.getInstance();

        int style = AlertDialog.THEME_HOLO_LIGHT;

        if ((day == null || day.isEmpty()) && (month == null || month.isEmpty()) && (year == null || year.isEmpty())) {
            int localYear, localMonth, localDay;

            localYear = cal.get(Calendar.YEAR);
            localMonth = cal.get(Calendar.MONTH);
            localDay = cal.get(Calendar.DAY_OF_MONTH);

            datePickerDialog = new DatePickerDialog(view.getContext(), style, dateSetListener, localYear, localMonth, localDay);
            datePickerDialog.getDatePicker().setMaxDate(cal.getTimeInMillis());
        }
        else {
            int[] date = makeDateInt(month, day, year);
            if (date[0] < 0 || date[0] > 11)
                date[0] = cal.get(Calendar.MONTH);
            if (date[1] < 1 || date[1] > 31)
                date[1] = cal.get(Calendar.DAY_OF_MONTH);
            if (date[2] < 1900 || date[2] > cal.get(Calendar.YEAR))
                date[2] = cal.get(Calendar.YEAR);

            datePickerDialog = new DatePickerDialog(view.getContext(), style, dateSetListener, date[2], date[0], date[1]);
            datePickerDialog.getDatePicker().setMaxDate(cal.getTimeInMillis());
        }
    }

    private int[] makeDateInt(String month, String day, String year) {
        int[] date = new int[3];
        String[] months = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

        if (month == null) date[0] = 0;
        else {
            date[0] = Arrays.asList(months).indexOf(month);
            if (date[0] == -1) date[0] = 0;
        }

        try {
            date[1] = Integer.parseInt(day);
            date[2] = Integer.parseInt(year);
        } catch (NumberFormatException e) {
            Log.e("DateConversionError", "Erreur lors de la conversion de la date en entier");
            Calendar cal = Calendar.getInstance();
            date[1] = cal.get(Calendar.DAY_OF_MONTH);
            date[2] = cal.get(Calendar.YEAR);
        }

        return date;
    }

    public void initialise() {
        //region Connection
        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
        //endregion

        //region InitDatePicker
        dateButton = view.findViewById(R.id.birthdateAddData);
        dateButton.setText(getTodayDate());
        //endregion

        //region InitHeight&WeightSeekBar
        weightBar = view.findViewById(R.id.weightAddData);
        heightBar = view.findViewById(R.id.heightAddData);
        weightText = view.findViewById(R.id.weightTextAddData);
        heightText = view.findViewById(R.id.heightTextAddData);
        initSeekBar();
        //endregion

        register = view.findViewById(R.id.saveAddData);
        sex = view.findViewById(R.id.sexAddData);
        male = view.findViewById(R.id.maleAddData);
        female = view.findViewById(R.id.femaleAddData);
    }

    private void initSeekBar() {
        heightBar.setMax(maxHeight-minHeight);

        heightBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int currentHeight = minHeight + progress;
                heightText.setText(currentHeight + " cm");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        weightBar.setMax(maxWeight - minWeight);

        weightBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int currentWeight = minWeight + progress;
                weightText.setText(currentWeight + " kg");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private String getTodayDate() {
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        return makeDateString(day, month, year);
    }

    private String makeDateString(int day, int month, int year) {
        return getMonthFormat(month) + " " + day + " " + year;
    }

    private String getMonthFormat(int month) {
        switch (month) {
            case 0:
                return "Jan";
            case 1:
                return "Feb";
            case 2:
                return "Mar";
            case 3:
                return "Apr";
            case 4:
                return "May";
            case 5:
                return "Jun";
            case 6:
                return "Jul";
            case 7:
                return "Aug";
            case 8:
                return "Sep";
            case 9:
                return "Oct";
            case 10:
                return "Nov";
            case 11:
                return "Dec";
            default:
                return "Jan";
        }
    }
}