package com.esaip.projetmob.main;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.esaip.projetmob.R;

public class WeightHeightFragment extends Fragment {

    private EditText editTextWeight, editTextHeight;
    private Button submitButton;

    public WeightHeightFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_weight_height, container, false);

        editTextWeight = view.findViewById(R.id.editTextWeight);
        editTextHeight = view.findViewById(R.id.editTextHeight);
        submitButton = view.findViewById(R.id.submitButton);

        submitButton.setOnClickListener(v -> submitWeightHeight());

        return view;
    }

    private void submitWeightHeight() {
        String weight = editTextWeight.getText().toString().trim();
        String height = editTextHeight.getText().toString().trim();

        if (weight.isEmpty() || height.isEmpty()) {
            Toast.makeText(getContext(), "Veuillez remplir tous les champs", Toast.LENGTH_SHORT).show();
            return;
        }

        // Traitement des données, comme les enregistrer dans la base de données
        Toast.makeText(getContext(), "Poids et taille enregistrés", Toast.LENGTH_SHORT).show();
    }
}
