package com.esaip.projetmob.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.esaip.projetmob.R;
import com.esaip.projetmob.connection.Connection;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class Data extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    private static FirebaseAuth mAuth;
    private static FirebaseUser user;
    private static String uid;

    private DrawerLayout drawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data);

        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();
        if (user != null) {
            uid = user.getUid();
        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawerLayout = findViewById(R.id.viewData_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(Data.this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open_nav, R.string.close_nav);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_containerChart, new ChartFragment()).commit();
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_containerData, new DataFragment()).commit();
            navigationView.setCheckedItem(R.id.nav_viewData);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.nav_home) {
            Intent i = new Intent(Data.this, MainActivity.class);
            i.putExtra("target", String.valueOf(R.id.nav_home));
            startActivity(i);
            finish();
        }
        else if (item.getItemId() == R.id.nav_addData) {
            Intent i = new Intent(Data.this, MainActivity.class);
            i.putExtra("target", String.valueOf(R.id.nav_addData));
            startActivity(i);
            finish();
        }
        else if (item.getItemId() == R.id.nav_viewData) {
            Intent i = new Intent(Data.this, Data.class);
            startActivity(i);
            finish();
        }
        else if (item.getItemId() == R.id.nav_meal) {
            Intent i = new Intent(Data.this, Data.class);
            i.putExtra("target", String.valueOf(R.id.nav_meal));
            startActivity(i);
            finish();
        }
        else if (item.getItemId() == R.id.nav_settings) {
            Intent i = new Intent(Data.this, MainActivity.class);
            i.putExtra("target", String.valueOf(R.id.nav_settings));
            startActivity(i);
            finish();
        }
        else if (item.getItemId() == R.id.nav_share) {
            Intent i = new Intent(Data.this, MainActivity.class);
            i.putExtra("target", String.valueOf(R.id.nav_share));
            startActivity(i);
            finish();
        }
        else if (item.getItemId() == R.id.nav_aboutus) {
            Intent i = new Intent(Data.this, MainActivity.class);
            i.putExtra("target", String.valueOf(R.id.nav_aboutus));
            startActivity(i);
            finish();
        }
        else if (item.getItemId() == R.id.nav_logout) {
            Toast.makeText(this, "Logout", Toast.LENGTH_SHORT).show();
            FirebaseAuth.getInstance().signOut();
            Intent i = new Intent(this, Connection.class);
            startActivity(i);
            finish();
        }

        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        }
        else {
            super.onBackPressed();
        }
    }
}