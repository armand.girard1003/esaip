package com.esaip.projetmob.main;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.esaip.projetmob.R;
import com.esaip.projetmob.database.ContractBdd;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class DataFragment extends Fragment {

    private View view;

    private static FirebaseAuth mAuth;
    private static FirebaseUser user;
    private static String uid;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_data, container, false);

        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();
        if (user != null) {
            uid = user.getUid();
            String[] currentData = new String[4];
            currentData = ContractBdd.getCurrentdata(uid, view.getContext());

            if (currentData[0]!= null) {
                TextView sexTextView = view.findViewById(R.id.sexData);
                sexTextView.setText(currentData[0]);
            }

            if (currentData[1]!= null) {
                TextView ageTextView = view.findViewById(R.id.ageData);
                ageTextView.setText(currentData[1]);
            }

            if (currentData[2]!= null) {
                TextView heightTextView = view.findViewById(R.id.heightData);
                heightTextView.setText(currentData[2]);
            }

            if (currentData[3]!= null) {
                TextView weightTextView = view.findViewById(R.id.weightData);
                weightTextView.setText(currentData[3]);
            }
        }

        // Inflate the layout for this fragment
        return view;
    }
}