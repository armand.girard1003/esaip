package com.esaip.projetmob.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Cette classe permet d'accéder à une base de données SQLite nommée DevMob
 * */
public class BddHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "DevMob.db";

    /**
     * Constructeur de la classe
     *
     * @param context Contexte d'appel du constructeur
     * */
    public BddHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(ContractBdd.SQL_CREATE_USERS);
        db.execSQL(ContractBdd.SQL_CREATE_USERDATA);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL(ContractBdd.SQL_DELETE_USERDATA);
        db.execSQL(ContractBdd.SQL_DELETE_USERS);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
