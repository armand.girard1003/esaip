CREATE DATABASE IF NOT EXISTS `examenira` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */;
USE `examenira`;

-- Listage de la structure de table examenira. compte
CREATE TABLE IF NOT EXISTS `compte` (
  `id_compte` int(11) NOT NULL AUTO_INCREMENT,
  `nom` text DEFAULT NULL,
  `login` text DEFAULT NULL,
  `mdp` text DEFAULT NULL,
  KEY `Index 1` (`id_compte`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Listage de la structure de table examenira. evenement
CREATE TABLE IF NOT EXISTS `evenement` (
  `idEvenement` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` text DEFAULT NULL,
  `lieu` text DEFAULT NULL,
  `dateEvenement` date DEFAULT NULL,
  KEY `Index 1` (`idEvenement`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;