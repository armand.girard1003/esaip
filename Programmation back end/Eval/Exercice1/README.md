J'ai fais une page d'accueil, permettant de choisir entre la connexion ou la création d'un compte.

La connexion d'un compte envoie sur une page de login avec un login et un mdp a entrer.

La création d'un compte envoie sur une page de création, avec un nom, un login, et un mdp à entrer.

Une fois cela fais, on nous envoie sur la page d'accueil personnalisée, avec un tableau contenant les événements.

Une fois le choix d'événement soumis, on nous envoie sur une page de confirmation.

Si on confirme, on nous envoie sur une page de fin.

Si on annule on nous renvoie sur la page d'accueil. 