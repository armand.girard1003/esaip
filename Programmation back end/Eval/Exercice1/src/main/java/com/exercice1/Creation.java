package com.exercice1;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Servlet implementation class Creation
 */
public class Creation extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	/**
	 * Attribut contenant l'instance de connexion à la base de données.
	 * */
	private Connection connexion;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Creation() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
    public void init(ServletConfig config) throws ServletException {
    	// Informations de connexion à la BDD
		String url = "jdbc:mysql://localhost:3306/examenira";
	    String user = "backend";
	    String password = "root";

	    try {
	        // Chargement du driver JDBC pour MySQL
	        Class.forName("com.mysql.cj.jdbc.Driver");
	        // Établissement de la connexion à la base de données
	        connexion = DriverManager.getConnection(url, user, password);
	        System.out.println("Connexion à la base de données établie.");
	    } catch (ClassNotFoundException | SQLException e) {
	        throw new ServletException("Impossible d'établir la connexion à la base de données", e);
	    }
	}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
		try {
	        if (connexion != null) {
	        	// Destruction de la connexion à la BDD.
	        	connexion.close();
	        }
	    } catch (SQLException e) {
	        e.printStackTrace();
	    }
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Récupération des informations de création de compte.
		String nom = request.getParameter("nom");
		String login = request.getParameter("login");
		String mdp = request.getParameter("mdp");
		
		// Création de la requête SQL.
		String reqSQL = "INSERT INTO Compte (login, mdp, nom) VALUES (?, ?, ?)";
		
		// Création de la liste des événements.
		List<EvenementClass> evenements = new ArrayList<>();
		try {
			// Préparation de la requête SQL d'insertion de compte.
			PreparedStatement status = connexion.prepareStatement(reqSQL);
			status.setString(1, login);
			status.setString(2, mdp);
			status.setString(3, nom);
			
			// Vérification
			int verif = status.executeUpdate();
			if (verif > 0) {
				//En cas de succès.
                request.setAttribute("login", nom);
                request.setAttribute("login", login);
                request.setAttribute("nom", nom);
                
                // Récupération des informations des événements.
                reqSQL = "SELECT * FROM evenement";
                status = connexion.prepareStatement(reqSQL);
                ResultSet res = status.executeQuery();
                
                while (res.next()) {
                    int id = res.getInt("idEvenement");
                    String libelle = res.getString("libelle");
                    String lieu = res.getString("lieu");
                    Date date = res.getDate("dateEvenement");

                    EvenementClass evenement = new EvenementClass(id, libelle, lieu, date);
                    evenements.add(evenement);
                }

                request.setAttribute("evenements", evenements);
                
                // Créer une session et stocker les informations de l'utilisateur
                HttpSession session = request.getSession();
                session.setAttribute("nom", nom);
                session.setAttribute("login", login);

                request.getRequestDispatcher("/Accueil.jsp").forward(request, response);
            } else {
                System.out.println("Échec de l'ajout de l'utilisateur.");
            }
			
			if (status != null) 
				status.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
