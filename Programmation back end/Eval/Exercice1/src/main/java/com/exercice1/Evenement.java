package com.exercice1;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Servlet implementation class Evenement
 */
public class Evenement extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private Connection connexion;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Evenement() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
    public void init(ServletConfig config) throws ServletException {
		String url = "jdbc:mysql://localhost:3306/examenira";
	    String user = "backend";
	    String password = "root";

	    try {
	        // Chargement du driver JDBC pour MySQL
	        Class.forName("com.mysql.cj.jdbc.Driver");
	        // Établissement de la connexion à la base de données
	        connexion = DriverManager.getConnection(url, user, password);
	        System.out.println("Connexion à la base de données établie.");
	    } catch (ClassNotFoundException | SQLException e) {
	        throw new ServletException("Impossible d'établir la connexion à la base de données", e);
	    }
	}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
		try {
	        if (connexion != null) {
	        	connexion.close();
	        }
	    } catch (SQLException e) {
	        e.printStackTrace();
	    }
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Statement status = null;
        List<EvenementClass> evenements = new ArrayList<>();
        
        try {
        	status = connexion.createStatement();
            String sql;
            sql = "SELECT * FROM evenement";
            ResultSet res = status.executeQuery(sql);

            while(res.next()) {
                int idEvenement = res.getInt("idEvenement");
                String libelle = res.getString("libelle");
                String lieu = res.getString("lieu");
                Date dateEvenement = res.getDate("dateEvenement");

                EvenementClass evenement = new EvenementClass(idEvenement, libelle, lieu, dateEvenement);
                evenements.add(evenement);
            }
            res.close();
            status.close();
            connexion.close();

            request.setAttribute("evenements", evenements);
            request.getRequestDispatcher("Accueil.jsp").forward(request, response);
            
            if(status != null) 
            	status.close();
        } catch(SQLException se) {
            se.printStackTrace();
        }
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
