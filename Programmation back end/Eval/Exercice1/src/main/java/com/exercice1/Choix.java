package com.exercice1;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Servlet implementation class Choix
 */
public class Choix extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	private String idEvenement;
	
	/**
	 * Attribut contenant l'instance de connexion à la base de données.
	 * */
	private Connection connexion;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Choix() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    /**
	 * @see Servlet#init(ServletConfig)
	 */
    public void init(ServletConfig config) throws ServletException {
    	// Informations de connexion à la BDD
		String url = "jdbc:mysql://localhost:3306/examenira";
	    String user = "backend";
	    String password = "root";

	    try {
	        // Chargement du driver JDBC pour MySQL
	        Class.forName("com.mysql.cj.jdbc.Driver");
	        // Établissement de la connexion à la base de données
	        connexion = DriverManager.getConnection(url, user, password);
	        System.out.println("Connexion à la base de données établie.");
	    } catch (ClassNotFoundException | SQLException e) {
	        throw new ServletException("Impossible d'établir la connexion à la base de données", e);
	    }
	}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
		try {
	        if (connexion != null) {
	        	// Destruction de la connexion à la BDD.
	        	connexion.close();
	        }
	    } catch (SQLException e) {
	        e.printStackTrace();
	    }
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Récupération du choix.
		String choix = request.getParameter("choix");
		
		switch(choix) {
		case "Confirmer":
			// Dans le cas où l'utilsateur confirme son choix.
			response.getWriter().println("Evenement selectionne : ID " + this.idEvenement);
			break;
		case "Annuler":
			// Dans le cas où l'utilsateur annule son choix.
			// Récupération des informations de la table Evenements.
			String sql = "SELECT * FROM evenement";
			List<EvenementClass> evenements = new ArrayList<>();
			try {
				PreparedStatement status = connexion.prepareStatement(sql);
				ResultSet res = status.executeQuery();
	            
	            while (res.next()) {
	                int id = res.getInt("idEvenement");
	                String libelle = res.getString("libelle");
	                String lieu = res.getString("lieu");
	                Date date = res.getDate("dateEvenement");

	                EvenementClass evenement = new EvenementClass(id, libelle, lieu, date);
	                evenements.add(evenement);
	            }

	            // Passer la liste d'événements à Accueil.jsp pour affichage
	            request.setAttribute("evenements", evenements);
	            request.getRequestDispatcher("/Accueil.jsp").forward(request, response);

			} catch (SQLException e) {
	            e.printStackTrace();
	        }
			break;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Transfert à la page de confirmation.
		this.idEvenement = request.getParameter("choix");
		request.getRequestDispatcher("/confirmation.jsp").forward(request, response);
    }

}
