package com.exercice1;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Servlet implementation class Connexion
 */
public class Connexion extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	/**
	 * Attribut contenant l'instance de connexion à la base de données.
	 * */
	private Connection connexion;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Connexion() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// Information de connexion à la base de données.
		String url = "jdbc:mysql://localhost:3306/examenira";
	    String user = "backend";
	    String password = "root";

	    try {
	        // Chargement du driver JDBC pour MySQL
	        Class.forName("com.mysql.cj.jdbc.Driver");
	        // Établissement de la connexion à la base de données
	        connexion = DriverManager.getConnection(url, user, password);
	    } catch (ClassNotFoundException | SQLException e) {
	        throw new ServletException("Impossible d'établir la connexion à la base de données", e);
	    }
	}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
		try {
	        if (connexion != null) {
	        	connexion.close();
	        }
	    } catch (SQLException e) {
	        e.printStackTrace();
	    }
	}
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Récupération des informations login et mdp.
		String login = request.getParameter("login");
		String mdp = request.getParameter("mdp");
		
		// Création de la liste des Evénements.
		List<EvenementClass> evenements = new ArrayList<>();
		
		try {
			// Check si les informations login et mdp correspondent ou non à une ligne dans la table compte.
            String reqSQL = "SELECT * FROM compte WHERE login = ? AND mdp = ? LIMIT 1";
            PreparedStatement status = connexion.prepareStatement(reqSQL);
            status.setString(1, login);
            status.setString(2, mdp);
            ResultSet res = status.executeQuery();

            if (res.next()) {
            	// Dans le cas où les informations de connexions correpondent à un compte.
                String nom = res.getString("nom");

                // Créer une session et stocker les informations de l'utilisateur
                HttpSession session = request.getSession();
                session.setAttribute("nom", nom);
                session.setAttribute("login", login);
                
                // Récupération des evénements dans la BDD.
                reqSQL = "SELECT * FROM evenement";
                status = connexion.prepareStatement(reqSQL);
                res = status.executeQuery();
                
                while (res.next()) {
                    int id = res.getInt("idEvenement");
                    String libelle = res.getString("libelle");
                    String lieu = res.getString("lieu");
                    Date date = res.getDate("dateEvenement");

                    EvenementClass evenement = new EvenementClass(id, libelle, lieu, date);
                    evenements.add(evenement);
                }

                request.setAttribute("evenements", evenements);
                session.setAttribute("evenements", evenements);
                
                if (res != null) 
                	res.close();
                if (status != null) 
                	status.close();
                
                // Envoie les informations à la page Accueil.jsp.
                request.getRequestDispatcher("/Accueil.jsp").forward(request, response);
            } else {
            	request.setAttribute("login", mdp);
                request.setAttribute("login", login);

                request.getRequestDispatcher("/ErreurConnexion.jsp").forward(request, response);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
