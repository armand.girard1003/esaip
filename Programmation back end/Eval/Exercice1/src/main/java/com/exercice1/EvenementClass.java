package com.exercice1;

import java.sql.Date;

public class EvenementClass {
	/**
	 * Attribut contenant l'ID de l'Evénement.
	 * */
    private int idEvenement;
    /**
	 * Attribut contenant le libellé de l'Evénement.
	 * */
    private String libelle;
    /**
	 * Attribut contenant le lieu de l'Evénement.
	 * */
    private String lieu;
    /**
	 * Attribut contenant la date de l'Evénement.
	 * */
    private Date dateEvenement;

    /**
     * Constructeur par défaut.
     * */
    public EvenementClass(int id, String libelle, String lieu, Date date) {
        this.idEvenement = id;
        this.libelle = libelle;
        this.lieu = lieu;
        this.dateEvenement = date;
    }
    
    // Getters des attributs.
    public int getIdEvenement() {
    	return this.idEvenement;
    }
    public String getLibelle() {
    	return this.libelle;
    }
    public String getLieu() {
    	return this.lieu;
    }
    public Date getDateEvenement() {
    	return this.dateEvenement;
    }
}
