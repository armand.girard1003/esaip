<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Erreur de connexion</title>
</head>
<body>
    <h2>Le login ${login} et le mot de passe ${mdp} ne correspondent à aucun compte.</h2>
    <br><br>
    <p><a href="/Exercice1">Se déconnecter</a></p>
</body>
</html>