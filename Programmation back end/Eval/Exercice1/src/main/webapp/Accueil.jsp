<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.List" %>
<%@ page import="com.exercice1.EvenementClass" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Accueil</title>
</head>
<body>
    <h1>Bienvenue, ${login}</h1>
    <p>Connexion au compte / Création du compte réussie ! Voici vos informations :</p>
    <ul>
        <li><strong>Login:</strong> ${login}</li>
        <li><strong>Nom Complet:</strong> ${nom}</li>
    </ul>
    <br><br>
    <p><a href="/Exercice1">Se déconnecter</a></p>
    
    <h1>Liste des événements</h1>
    
    <form method="post" action="Choix">
	    <table border="1">
	        <thead>
	            <tr>
	                <th>ID</th>
	                <th>Libellé</th>
	                <th>Lieu</th>
	                <th>Date</th>
	            </tr>
	        </thead>
	        <tbody>
	            <% 
	            List<EvenementClass> evenements = (List<EvenementClass>) request.getAttribute("evenements");
	            if (evenements != null && !evenements.isEmpty()) {
	                for (EvenementClass evenement : evenements) {
	            %>
	            <tr>
	                <td><%= evenement.getIdEvenement() %></td>
	                <td><%= evenement.getLibelle() %></td>
	                <td><%= evenement.getLieu() %></td>
	                <td><%= evenement.getDateEvenement() %></td>
	                <td><input type="radio" name="choix" value="<%= evenement.getIdEvenement() %>"></td>
	            </tr>
	            <% 
	                }
	            } else {
	            %>
	            <tr>
	                <td colspan="4">Aucun événement trouvé.</td>
	            </tr>
	            <% } %>
	        </tbody>
	    </table>
    	<br>
     	<input type="submit" value="Sélectionner">
    </form>
    
    <br><br><br>
    <a href="/Exercice1">Retour à la page principale</a>
</body>
</html>