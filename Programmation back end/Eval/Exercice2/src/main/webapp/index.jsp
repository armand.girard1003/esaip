<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Exercice 2 - La Bibliothèque</title>
	</head>
	<body>
		<h1>Insérez un livre dans la bibliothèque : </h1>
		<br>
		<form method="post" action="Bibliotheque" id="livre" name="livre">
			<label>Entrez titre de livre : </label>
			<input id="titre" name="titre" type="text" value="">
			<br>
			<label>Entrez l'auteur du livre : </label>
			<input id="auteur" name="auteur" type="text" value="">
			<br>
			<label>Entrez l'année de parution du livre : </label>
			<input id="annee" name="annee" type="text" value="">
			<br>
			<label>Entrez le prix du livre : </label>
			<input id="prix" name="prix" type="text" value="">
			<br>
			<label>Entrez la quantité en stock du livre : </label>
			<input id="qte_stock" name="qte_stock" type="text" value="">
			<br>
			<input type="submit" value="Insérer">
		</form>
	</body>
</html>