<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>Erreur</title>
</head>
<body>
    <h1>Une erreur s'est produite</h1>
    <p>${erreur}</p>
    <p><a href="/Exercice2">Retour à la page d'ajout de livre</a></p>
</body>
</html>