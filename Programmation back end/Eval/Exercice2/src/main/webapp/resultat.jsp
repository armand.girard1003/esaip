<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.exercice2.Livre" %>
<%@ page import="java.util.List, java.util.ArrayList" %>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Résultat de la recherche</title>
	</head>
	<body>
		<h1>Résultats de la Recherche</h1>
		<table border="1">
	        <thead>
	            <tr>
	                <th>ID Livre</th>
	                <th>Titre</th>
	                <th>Auteur</th>
	                <th>Année</th>
	                <th>Prix</th>
	                <th>Quantité en Stock</th>
	            </tr>
	        </thead>
	        <tbody>
	            <% 
	            List<Livre> livres = (List<Livre>) request.getAttribute("livres");
	            if (livres != null && !livres.isEmpty()) {
	                for (Livre livre : livres) {
	            %>
	            <tr>
	                <td><%= livre.getIdLivre() %></td>
	                <td><%= livre.getTitre() %></td>
	                <td><%= livre.getAuteur() %></td>
	                <td><%= livre.getAnnee() %></td>
	                <td><%= livre.getPrix() %></td>
	                <td><%= livre.getQteStock() %></td>
	            </tr>
	            <% 
	                }
	            } else {
	            %>
	            <tr>
	                <td colspan="6">Aucun livre trouvé.</td>
	            </tr>
	            <% } %>
	        </tbody>
	    </table>
	    <br><br><br>
		<a href="/Exercice2">Aller vers la page principale.</a>
	</body>
</html>