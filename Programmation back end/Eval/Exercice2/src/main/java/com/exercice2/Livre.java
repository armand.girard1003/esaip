package com.exercice2;

public class Livre {
    private int idLivre;
    private String titre;
    private String auteur;
    private String annee;
    private float prix;
    private int qteStock;

    // Constructeur par défaut
    public Livre() {
    }

    // Getters et Setters pour les attributs
    public int getIdLivre() {
        return idLivre;
    }

    public void setIdLivre(int idLivre) {
        this.idLivre = idLivre;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public String getAnnee() {
        return annee;
    }

    public void setAnnee(String annee) {
        this.annee = annee;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }

    public int getQteStock() {
        return qteStock;
    }

    public void setQteStock(int qteStock) {
        this.qteStock = qteStock;
    }

    // Méthode toString() pour l'affichage
    @Override
    public String toString() {
        return "Livre [idLivre=" + idLivre + ", titre=" + titre + ", auteur=" + auteur + ", annee=" + annee
                + ", prix=" + prix + ", qteStock=" + qteStock + "]";
    }
}

