package com.exercice2;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Servlet implementation class Connexion
 */
public class Bibliotheque extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	/**
	 * Attribut contenant l'instance de la connexion à la base de données.
	 * */
	private Connection connexion;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Bibliotheque() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// Informations de connexion à la base de données.
		String url = "jdbc:mysql://localhost:3306/bibliotheque"; 
	    String user = "backend";
	    String password = "root";

	    try {
	        // Chargement du driver JDBC pour MySQL
	        Class.forName("com.mysql.cj.jdbc.Driver");
	        // Établissement de la connexion à la base de données
	        connexion = DriverManager.getConnection(url, user, password);
	    } catch (ClassNotFoundException | SQLException e) {
	        throw new ServletException("Impossible d'établir la connexion à la base de données", e);
	    }
	}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
		try {
	        if (connexion != null) {
	        	// Destruction de l'instance de connexion à la base de données.
	        	connexion.close();
	        }
	    } catch (SQLException e) {
	        e.printStackTrace();
	    }
	}
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Informations du livre à insérer dans la base de données.
	    String titre = request.getParameter("titre");
	    String auteur = request.getParameter("auteur");
	    String annee = request.getParameter("annee");
	    String prix = request.getParameter("prix");
	    String qte_stock = request.getParameter("qte_stock");

	    // Création de la requête SQL.
	    String reqSQL = "INSERT INTO livres (titre, auteur, annee, prix, qte_stock) VALUES (?, ?, ?, ?, ?)";

	    try {
	    	// Préparation de la requête SQL.
	        PreparedStatement status = connexion.prepareStatement(reqSQL);
	        status.setString(1, titre);
	        status.setString(2, auteur);
	        status.setString(3, annee);
	        status.setString(4, prix);
	        status.setString(5, qte_stock);

	        // Vérification de la requête SQL.
	        int verif = status.executeUpdate();
	        if (verif > 0) {
	            // En cas de succès.
	        	try {
	        		// Préapration et exécution de la requête SQL pour récupérer le contenu de la table.
	                reqSQL = "SELECT * FROM Livres";
	                PreparedStatement status2 = connexion.prepareStatement(reqSQL);
	                ResultSet resultSet = status2.executeQuery();
	                
	                // Création de la liste de Livres récupérés.
	                List<Livre> livres = new ArrayList<>();
	                while (resultSet.next()) {
	                	// Récupération des informations des livres récupérés.
	                    Livre livre = new Livre();
	                    livre.setIdLivre(resultSet.getInt("idLivre"));
	                    livre.setTitre(resultSet.getString("titre"));
	                    livre.setAuteur(resultSet.getString("auteur"));
	                    livre.setAnnee(resultSet.getString("annee"));
	                    livre.setPrix(resultSet.getFloat("prix"));
	                    livre.setQteStock(resultSet.getInt("qte_stock"));
	                    livres.add(livre);
	                }
	                
	                // Envoie des informations à resultat.jsp.
	                request.setAttribute("livres", livres);
	                resultSet.close();
	                status2.close();
	                request.getRequestDispatcher("resultat.jsp").forward(request, response);
	            } catch (SQLException e) {
	            	// En cas d'erreur de Sélection.
	                e.printStackTrace();
	                String errorMessage = "Erreur lors de la récupération des livres. Veuillez réessayer.";
	                request.setAttribute("erreur", errorMessage);
	                request.getRequestDispatcher("erreur.jsp").forward(request, response);
	            }
	        } else {
	        	// En cas d'échec d'Insertion.
	            String errorMessage = "Aucune ligne insérée. Veuillez réessayer.";
	            request.setAttribute("erreur", errorMessage);
	            request.getRequestDispatcher("erreur.jsp").forward(request, response);
	        }
	    } catch (SQLException e) {
	    	// En cas d'erreur.
	        e.printStackTrace();
	        String errorMessage = "Erreur lors de l'insertion du livre : " + e.getMessage();
	        request.setAttribute("erreur", errorMessage);
	        request.getRequestDispatcher("erreur.jsp").forward(request, response);
	    }
	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
