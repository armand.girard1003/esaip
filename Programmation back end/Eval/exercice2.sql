CREATE DATABASE IF NOT EXISTS `bibliotheque` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */;
USE `bibliotheque`;

-- Listage de la structure de table bibliotheque. livres
CREATE TABLE IF NOT EXISTS `livres` (
  `idLivre` int(11) NOT NULL AUTO_INCREMENT,
  `titre` text DEFAULT NULL,
  `auteur` text DEFAULT NULL,
  `annee` text DEFAULT NULL,
  `prix` float DEFAULT NULL,
  `qte_stock` int(11) DEFAULT NULL,
  KEY `Index 1` (`idLivre`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;