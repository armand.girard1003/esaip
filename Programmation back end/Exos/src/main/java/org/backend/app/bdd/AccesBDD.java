package org.backend.app.bdd;

import java.sql.*;

public class AccesBDD {
	public static void main(String[] args) throws SQLException {
        DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());

        Connection connect = DriverManager.getConnection("jdbc:mysql://localhost/test_backend?user=root&password=");
        Statement stmt = connect.createStatement();
        
        String sql_select = "Select * From test;";
        
        stmt.execute(sql_select);
        ResultSet rset = stmt.getResultSet();
        int key = 0;
        while(rset.next()) {
        	System.out.println("key : " + rset.getInt("key"));
        	System.out.println("Nom : " + rset.getString("Nom"));
        	System.out.println("Prenom : " + rset.getString("Prenom"));
        	System.out.println("Age : " + rset.getInt("Age"));
        	System.out.println("Date : " + rset.getString("Date"));
        	System.out.println("----------------------------");
        	rset.getInt("key");
        }
        
        String sql_insert = "Insert into test Values (" + Integer.toString(key) + ", 'Gaudin', 'Raphael', 20, '20/07/2003');";
        stmt.execute(sql_insert);
    }
}
