package org.bakend.test;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Servlet implementation class CompteurVisites
 */
public class CompteurVisites extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CompteurVisites() {
        super();
        // TODO Auto-generated constructor stub
    }
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Récupérer le cookie existant ou créer un nouveau cookie
        Cookie[] cookies = request.getCookies();
        int visitsCount = 1;
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("visitsCount")) {
                    visitsCount = Integer.parseInt(cookie.getValue());
                    visitsCount++;
                    cookie.setValue(String.valueOf(visitsCount));
                    response.addCookie(cookie);
                    break;
                }
            }
        }
        if (visitsCount == 1) {
            Cookie cookie = new Cookie("visitsCount", String.valueOf(visitsCount));
            response.addCookie(cookie);
        }

        // Afficher le nombre de visites
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<html><head><title>Visits Counter</title></head><body>");
        out.println("<h1>Nombre de visites : " + visitsCount + "</h1>");
        out.println("</body></html>");
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
