package org.bakend.test;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Servlet implementation class HelloWorld
 */
public class HelloWorld extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HelloWorld() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Récupérer les paramètres de la requête
        String responseGet = request.getParameter("responseGet");

        // Afficher les paramètres récupérés dans la console
        System.out.println("Response Get: " + responseGet);

        // Définir le type de contenu de la réponse comme HTML
        response.setContentType("text/html");

        // Écrire la réponse HTML
        response.getWriter().println(
        		"<html>" +
        			"<head>" + 
        				"<title>Réponse du formulaire</title>" + 
        			"<body>" + 
        				"<h1>Réponse du formulaire Get</h1>" + 
        				"<p>Votre réponse au formulaire est : " + responseGet + "</p>" +
        			"</body>" + 
        		"</html>"		
        		);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Récupérer les paramètres du formulaire
        String responsePost = request.getParameter("responsePost");

        // Définir le type de contenu de la réponse comme HTML
        response.setContentType("text/html");

        // Créer un PrintWriter pour écrire dans la réponse
        PrintWriter out = response.getWriter();

        // Écrire la réponse HTML
        out.println(
        		"<html>" +
        			"<head>" +
        				"<title>Réponse du formulaire Post</title>" + 
        			"</head>" +
        			"<body>" + 
        				"<h1>Réponse du formulaire</h1>" + 
        				"<p>Votre réponse au formulaire est : " + responsePost + "</p>" + 
        			"</body>" + 
        		"</html>"
        		);

        out.close();
	}

}
