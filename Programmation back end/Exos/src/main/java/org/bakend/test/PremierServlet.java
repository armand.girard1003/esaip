package org.bakend.test;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Servlet implementation class PremierServlet
 */
public class PremierServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PremierServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");

		PrintWriter out = response.getWriter();
		
		out.println(
				"<HTML>\n" + 
					"<BODY>\n" +
						"<H1>Recapitulatif des informations</H1>\n" +
						"<UL>\n" +			   
							"<LI>Nom: " + 
							request.getParameter("nom") + "\n" +
							"<LI>Prenom: " + 
							request.getParameter("prenom") + "\n" +
							"<LI>Age: " + 
							request.getParameter("age") + "\n" +
						"</UL>\n" +				
					"</BODY>" + 
				"</HTML>"); 
		doGet(request, response);
	}

}
