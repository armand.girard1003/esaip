<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>ACCUEIL</title>
	</head>
	<body>
		<h1>Accueil</h1>
		<p>
			<a href="liste.jsp">Liste des utilisateurs</a>
		</p>
		<p>
			<a href="ajouter.jsp">Ajouter un utilisateur</a>
		</p>
		<p>
			<a href="modifier.jsp">Modifier un utilisateur</a>
		</p>
		<p>
			<a href="supprimer.jsp">Supprimer un utilisateur</a>
		</p>
	</body>
</html>