package com.montruc.accueil;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Servlet implementation class Accueil
 */
public class Accueil extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private Connection connexion;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Accueil() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// Informations de connexion à la BDD
		String url = "jdbc:mysql://localhost:3306/examenira";
	    String user = "backend";
	    String password = "root";

	    try {
	    	// Chargement du driver JDBC pour MySQL
	    	Class.forName("com.mysql.cj.jdbc.Driver");
	    	// Établissement de la connexion à la base de données
	    	connexion = DriverManager.getConnection(url, user, password);
	    	System.out.println("Connexion à la base de données établie.");
	    } catch (ClassNotFoundException | SQLException e) {
	    	throw new ServletException("Impossible d'établir la connexion à la base de données", e);
	    }
	}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
		try {
	        if (connexion != null) {
	        	// Destruction de la connexion à la BDD.
	        	connexion.close();
	        }
	    } catch (SQLException e) {
	        e.printStackTrace();
	    }
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
