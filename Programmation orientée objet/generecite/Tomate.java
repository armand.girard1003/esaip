public class Tomate<L, J> implements Legume<L, J>{
    public String couleur;
    public L variete;
    public J jardin;

    public Tomate(L variete, String couleur, J jardin) {
        this.variete = variete;
        this.couleur = couleur;
        this.jardin = jardin;
    }

    public L getLegume() {
        return this.getVariete();
    }

    public J getAdresseJardin() {
        return this.getJardin();
    }

    //#region Getters and Setters
    public L getVariete() {return variete;}

    public void setVariete(L variete) {this.variete = variete;}

    public String getCouleur() {return couleur;}

    public void setCouleur(String couleur) {this.couleur = couleur;}

    public J getJardin() {return jardin;}

    public void setJardin(J jardin) {this.jardin = jardin;}
    //#endregion

    //#region toString
    @Override
    public String toString() {
        return "Tomate [variete=" + variete + ", couleur=" + couleur + "]";
    }
    //#endregion
}
