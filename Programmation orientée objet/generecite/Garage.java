public class Garage {
    public String nom;
    public String adresse;
    public int surface;

    public Garage(String nom, String adresse, int surface) {
        this.nom = nom;
        this.adresse = adresse;
        this.surface = surface;
    }

    //#region Getters and Setters
    public String getNom() {return nom;}

    public void setNom(String nom) {this.nom = nom;}

    public String getAdresse() {return adresse;}

    public void setAdresse(String adresse) {this.adresse = adresse;}

    public int getSurface() {return surface;}

    public void setSurface(int surface) {this.surface = surface;}
    //#endregion

    //#region toString
    @Override
    public String toString() {
        return "Garage [nom=" + nom + ", adresse=" + adresse + "]";
    }
    //#endregion
}
