public class Carotte<L, J> implements Legume<L, J> {
    public String nom;
    public L variete;
    public J jardin;

    public Carotte(String nom, L variete, J jardin) {
        this.nom = nom;
        this.variete = variete;
        this.jardin = jardin;
    }

    public L getLegume() {
        return this.getVariete();
    }

    public J getAdresseJardin() {
        return this.getJardin();
    }

    //#region Getters and Setters
    public String getNom() {return nom;}

    public void setNom(String couleur) {this.nom = couleur;}

    public L getVariete() {return variete;}

    public void setVariete(L variete) {this.variete = variete;}

    public J getJardin() {return jardin;}

    public void setJardin(J jardin) {this.jardin = jardin;}
    //#endregion

    //#region toString
    @Override
    public String toString() {
        return "Carotte [nom=" + nom + ", variete=" + variete + "]";
    }
    //#endregion
}
