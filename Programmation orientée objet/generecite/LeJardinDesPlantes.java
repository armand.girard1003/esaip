public class LeJardinDesPlantes {
    public String adresse;
    public String emplacement;

    public LeJardinDesPlantes(String adresse, String emplacement) {
        this.adresse = adresse;
        this.emplacement = emplacement;
    }

    //#region Getters and Setters
    public String getAdresse() {return adresse;}

    public void setAdresse(String adresse) {this.adresse = adresse;}

    public String getEmplacement() {return emplacement;}

    public void setEmplacement(String emplacement) {this.emplacement = emplacement;}
    //#endregion

    //#region toString
    @Override
    public String toString() {
        return "LeJardinDesPlantes [adresse=" + adresse + ", emplacement=" + emplacement + "]";
    }
    //#endregion
}
