public class Paire <T> {
    public T premier;
    public T second;

    public Paire(T premier, T second) {
        super();
        this.premier = premier;
        this.second = second;
    }


    //#region Getters and Setters
    public T getPremier() {return premier;}

    public void setPremier(T premier) {this.premier = premier;}

    public T getSecond() {return second;}

    public void setSecond(T second) {this.second = second;}
    //#endregion


    //#region toString
    @Override
    public String toString() {
        return "Paire [premier=" + premier + ", second=" + second + "]";
    }
    //#endregion
}
