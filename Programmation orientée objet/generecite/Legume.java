public interface Legume <L, J> {
    L getLegume();
    J getAdresseJardin();
}
