public class Plantation <T1, T2> {
    T1 typePlantation;
    T2 adresseJardin;

    public Plantation(T1 typePlantation, T2 adresseJardin) {
        this.typePlantation = typePlantation;
        this.adresseJardin = adresseJardin;
    }

    //#region Getters and Setters
    public T1 getTypePlantation() {return typePlantation;}

    public void setTypePlantation(T1 typePlantation) {this.typePlantation = typePlantation;}

    public T2 getAdresseJardin() {return adresseJardin;}

    public void setAdresseJardin(T2 adresseJardin) {this.adresseJardin = adresseJardin;}
    //#endregion

    //#region toString
    @Override
    public String toString() {
        return "Plantation [typePlantation=" + typePlantation + ", adresseJardin=" + adresseJardin + "]";
    }
    //#endregion
}
