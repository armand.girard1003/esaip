#include <SPI.h>
#include <MFRC522.h>

#define RST_PIN 9  // Broche de réinitialisation
#define SS_PIN 10  // Broche SS (SDA)

MFRC522 rfid(SS_PIN, RST_PIN);

void setup() {
  Serial.begin(9600); // Initialisation du port série
  SPI.begin();        // Initialisation de SPI
  rfid.PCD_Init();    // Initialisation du module RC522
  Serial.println("Scanner RFID prêt !");
}

void loop() {
  // Vérifie si une carte est présente
  if (!rfid.PICC_IsNewCardPresent()) {
    return;
  }

  // Vérifie si la carte peut être lue
  if (!rfid.PICC_ReadCardSerial()) {
    return;
  }

  // Affiche l'UID de la carte
  Serial.print("Carte détectée : ");
  for (byte i = 0; i < rfid.uid.size; i++) {
    Serial.print(rfid.uid.uidByte[i] < 0x10 ? " 0" : " ");
    Serial.print(rfid.uid.uidByte[i], HEX);
  }
  Serial.println();

  // Arrête la communication avec la carte
  rfid.PICC_HaltA();
}
