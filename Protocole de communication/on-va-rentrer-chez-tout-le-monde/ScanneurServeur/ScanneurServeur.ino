/********************************************************
Il y a plusieurs bibliothèques à installer :
MFRC522 de GitHub Community
WebSockets de Markus Sattler
LiquidCrystal I2C de Frank Brabander

Si vous n'avez pas de lecteur LCD ou de module WiFi, pas de soucis.
Le programme sera simplement un scanneur, sans utiliser le reste des fonctionnalités.
********************************************************/

#pragma region Includes

#include "SPI.h"
#include "WiFiS3.h"
#include "MFRC522.h"
#include <WebSocketsServer.h>
#include <LiquidCrystal_I2C.h>

#pragma endregion

#pragma region Defines

// Définition des broches pour RC522
#define RST_PIN 9
#define SS_PIN 10
#define NR_KNOWN_KEYS 8

// WiFi configuration
#define SECRET_SSID "Naa"
#define SECRET_PASS "armand2003"

// Ports utilisés
#define PORT_SERVERWEB 80
#define PORT_WEBSOCKET 81

#pragma endregion

#pragma region Outils

// Outils pour la connexion WiFi et le serveur HTTP
char ssid[] = SECRET_SSID;
char pass[] = SECRET_PASS;
WiFiServer httpServer(PORT_SERVERWEB);
bool wifiEnabled = false; // Détermine si le module WiFi est disponible

// Outils pour le module rc522
MFRC522 mfrc522(SS_PIN, RST_PIN);
byte buffer[18];
byte block;
byte waarde[64][16];
MFRC522::StatusCode status;
MFRC522::MIFARE_Key key;
byte knownKeys[NR_KNOWN_KEYS][MFRC522::MF_KEY_SIZE] =  {
    {0xff, 0xff, 0xff, 0xff, 0xff, 0xff}, // FF FF FF FF FF FF = factory default
    {0xa0, 0xa1, 0xa2, 0xa3, 0xa4, 0xa5}, // A0 A1 A2 A3 A4 A5
    {0xb0, 0xb1, 0xb2, 0xb3, 0xb4, 0xb5}, // B0 B1 B2 B3 B4 B5
    {0x4d, 0x3a, 0x99, 0xc3, 0x51, 0xdd}, // 4D 3A 99 C3 51 DD
    {0x1a, 0x98, 0x2c, 0x7e, 0x45, 0x9a}, // 1A 98 2C 7E 45 9A
    {0xd3, 0xf7, 0xd3, 0xf7, 0xd3, 0xf7}, // D3 F7 D3 F7 D3 F7
    {0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff}, // AA BB CC DD EE FF
    {0x00, 0x00, 0x00, 0x00, 0x00, 0x00}  // 00 00 00 00 00 00
};
String uidString = "Aucun tag scanné";

// Outil pour le WebSocket
WebSocketsServer webSocket(PORT_WEBSOCKET);

// Outils pour l'écran LCD
LiquidCrystal_I2C lcd(0x27,16,2);
bool lcdEnabled = true; // Détermine si le module LCD est disponible

#pragma endregion

void setup() {
  Serial.begin(9600);
  SPI.begin();

  handleMenu();

  #pragma region Initialisation de lécran

  if (i2CAddrTest(0x27) || i2CAddrTest(0x3F)) { // Check si module LCD est présent
    lcd.init();
    lcd.backlight();
    Serial.println("Freenove I2C LCD 1602 initialisé");
    if (lcdEnabled) showMessage("LCD OK", 2);
  } 
  else { // Si aucune module LCD présent
    Serial.println("Module LCD non disponible.");
    lcdEnabled = false;
  }

  #pragma endregion

  #pragma region Initialisation du RC522

  mfrc522.PCD_Init();
  Serial.println(F("RFID RC522 initialisé"));
  if (lcdEnabled) showMessage("RC522 OK", 2);

  #pragma endregion

  #pragma region Initialisation Wi-Fi

  if (WiFi.status() != WL_NO_MODULE) { // Check si le module WiFi est disponible
    unsigned long startAttemptTime = millis();
    while (WiFi.begin(ssid, pass) != WL_CONNECTED && millis() - startAttemptTime < 30000) { // Tentative de connexion
      Serial.println("Connexion Wi-Fi en cours...");
      delay(3000);
    }

    if (WiFi.status() == WL_CONNECTED) { // Check si la connexion au réseau est établie
      wifiEnabled = true;
      Serial.print("Connexion Wi-Fi réussie ! http://");
      Serial.println(WiFi.localIP());
      if (lcdEnabled) showMessage("WIFI OK", 2);
      httpServer.begin(); // Démarrage du server HTTP
      webSocket.begin(); // Démarrage du service WebSocket
      webSocket.onEvent(webSocketEvent);
      Serial.println("WebSocket initialisé");
    } 
    else { // Si la connexion au réseau échoue
      Serial.println("Échec de connexion Wi-Fi");
      if (lcdEnabled) showMessage("WIFI indispo", 2);
    }
  } 
  else { // Si aucun module WiFi disponible
    Serial.println("Module Wi-Fi non disponible.");
    if (lcdEnabled) showMessage("Module WIFI !ok", 2);
  }

  #pragma endregion

  Serial.println("Scanneur PRET");
  if (lcdEnabled) showMessage("Scanneur PRET", 2);
}

void loop() {
  handleChoice();
  if (wifiEnabled) {
    handleHttpServer();
    webSocket.loop();
  }
}

void handleChoice() {
  if (Serial.available() > 0) {
    char choix = Serial.read();
    switch(choix) {
      case "1":
        Serial.println("Vous avez choisi de lire un tag.");
        if (!mfrc522.PICC_IsNewCardPresent()) return;
        if (!mfrc522.PICC_ReadCardSerial()) return;
        Serial.println(F("Card detected."));
        handleScanner();
        handleMenu();
        break;
      case "2":
        Serial.println("Vous avez choisi de lire un tag.");
        if (!mfrc522.PICC_IsNewCardPresent()) return;
        if (!mfrc522.PICC_ReadCardSerial()) return;
        Serial.println(F("Card detected."));
        handleWriter();
        handleMenu();
        break;
      case "2":
        Serial.println("Vous avez choisi de lire un tag.");
        handleCloner();
        handleMenu();
        break;
      default:
        Serial.println("Choix invalide.");
        handleMenu();
        break;
    }
  } 
}

void handleMenu() {
  Serial.println("=== Menu ===");
  Serial.println("1. Lire un tag");
  Serial.println("2. Ecrire un tag");
  Serial.println("1. Cloner un tag");
  Serial.println("Veuillez entrer votre choix (1, 2 ou 3) : ");
}

/*
Affichage d'un message sur l'écran Freenove I2C LCD 1602
String message - message à afficher
unisgned int duration - durée d'affichage du message
*/
void showMessage(String message, unsigned int duration) {
  if (lcdEnabled) {
    lcd.clear();
    lcd.print(message);
    if (duration != 0) delay(duration * 1000);
  } else {
    Serial.println(message); // Afficher le message dans la console si le LCD est indisponible
  }
}

/*
Affichage de l'uid sur l'écran Freenove I2C LCD 1602
String uid - uid à afficher sur l'écran
*/
void showUidLCD(String uid) {
  if (lcdEnabled) {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Scan ok !");
    lcd.setCursor(0, 1);
    lcd.print("UID: " + uid);
  }
}

/*
Fonction appelée lors d'un scan
*/
void handleScanner() {
    byte sector = 1;
    byte blockAddr = 4;
    byte buffer[18];
    byte size = sizeof(buffer);
    MFRC522::StatusCode status;

    // Authenticate using key A
    Serial.println(F("Authenticating using key A..."));
    status = (MFRC522::StatusCode)mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, blockAddr, &key, &(mfrc522.uid));
    if (status != MFRC522::STATUS_OK) {
        Serial.print(F("Authentication failed: "));
        Serial.println(mfrc522.GetStatusCodeName(status));
        return;
    }

    // Read data from the block
    Serial.print(F("Reading data from block ")); Serial.print(blockAddr); Serial.println(F(" ..."));
    status = (MFRC522::StatusCode)mfrc522.MIFARE_Read(blockAddr, buffer, &size);
    if (status != MFRC522::STATUS_OK) {
        Serial.print(F("MIFARE_Read() failed: "));
        Serial.println(mfrc522.GetStatusCodeName(status));
        return;
    }

    // Display the read data
    Serial.print(F("Data in block ")); Serial.print(blockAddr); Serial.println(F(":"));
    dump_byte_array(buffer, 16);
    Serial.println();
}

/*
Fonction appelée pour écrire un tag
*/
void handleWriter() {
    byte sector = 1;
    byte blockAddr = 4;
    byte dataBlock[] = {
        0x01, 0x02, 0x03, 0x04,
        0x05, 0x06, 0x07, 0x08,
        0x09, 0x0A, 0xFF, 0x0B,
        0x0C, 0x0D, 0x0E, 0x0F
    };
    MFRC522::StatusCode status;

    // Authenticate using key B
    Serial.println(F("Authenticating using key B..."));
    status = (MFRC522::StatusCode)mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_B, blockAddr, &key, &(mfrc522.uid));
    if (status != MFRC522::STATUS_OK) {
        Serial.print(F("Authentication failed: "));
        Serial.println(mfrc522.GetStatusCodeName(status));
        return;
    }

    // Write data to the block
    Serial.print(F("Writing data to block ")); Serial.print(blockAddr); Serial.println(F(" ..."));
    dump_byte_array(dataBlock, 16);
    Serial.println();
    status = (MFRC522::StatusCode)mfrc522.MIFARE_Write(blockAddr, dataBlock, 16);
    if (status != MFRC522::STATUS_OK) {
        Serial.print(F("MIFARE_Write() failed: "));
        Serial.println(mfrc522.GetStatusCodeName(status));
        return;
    }

    Serial.println(F("Data written successfully."));
}

/*
Fonction appelée pour cloner un tag
*/
void handleCloner() {
  
}

/*
Fonction appelée si un client se connecte au server HTTP
*/
void handleHttpServer() {
  WiFiClient client = httpServer.available();
  if (client) {
    String header = "";
    while (client.connected()) {
      if (client.available()) {
        char c = client.read();
        header += c;
        if (c == '\n' && header.endsWith("\r\n\r\n")) {
          client.println("HTTP/1.1 200 OK");
          client.println("Content-type:text/html");
          client.println("Connection: close");
          client.println();
          client.println("<!DOCTYPE html><html>");
          client.println("<head><meta charset='utf-8'>");
          client.println("<title>Scan RFID</title>");
          client.println("</head><body>");
          client.println("<h1>Scanner RFID</h1>");
          client.println("</body></html>");
          break;
        }
      }
    }
    client.stop();
    Serial.println("Client déconnecté.");
  }
}

/*
Fonction appelée lors de la réception d'un message via le service WebSocket
*/
void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t length) {
  if (type == WStype_TEXT) {
    Serial.print("[");
    Serial.print(num);
    Serial.print("] Message reçu : ");
    Serial.println((char*)payload);
  }
}

/*
Fonction de test pour savoir si l'adresse I2C entrée est la bonne (soit 0x3F, soit 0x27)
uint8_t addr - Adresse I2C à tester
*/
bool i2CAddrTest(uint8_t addr) {
  Wire.begin();
  Wire.beginTransmission(addr);
  return (Wire.endTransmission() == 0);
}

/**
 * Helper routine to dump a byte array as hex values to Serial.
 */
void dump_byte_array(byte *buffer, byte bufferSize) {
    for (byte i = 0; i < bufferSize; i++) {
        Serial.print(buffer[i] < 0x10 ? " 0" : " ");
        Serial.print(buffer[i], HEX);
    }
}
