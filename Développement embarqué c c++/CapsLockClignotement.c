#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/kd.h>

#define LED_CAPS_LOCK 0x04  // Code de la LED CAPS LOCK

int main() {
    int fd;
    int state = 0;

    // Ouvrir le périphérique de la console
    fd = open("/dev/console", O_WRONLY);
    if (fd == -1) {
        perror("Erreur lors de l'ouverture de /dev/console");
        return 1;
    }

    printf("Clignotement de la LED Caps Lock...\n");

    for (int i = 0; i < 5; i++) { // Clignote 10 fois
        state = state ? 0 : LED_CAPS_LOCK; // Alterne entre éteint et allumé
        if (ioctl(fd, KDSETLED, state) == -1) {
            perror("Erreur lors du changement d'état de la LED");
            close(fd);
            return 1;
        }
        sleep(2); // Pause de 2s
    }

    // Éteindre la LED avant de quitter
    ioctl(fd, KDSETLED, 0);
    close(fd);

    printf("Fin du programme.\n");

    return 0;
}
