#include <stdio.h>

const char g_tc_phrase[124] = "Lorem ipsum dolor sit amet, " \
                           "consectetur adipiscing elit, " \
                           "sed do eiusmod tempor incididunt " \
                           "ut labore et dolore magna aliqua.";
const char g_c_token = 'r';
const int g_i_longueurPhrase = 123;
int g_i_nombreOccurrence = 0;

int main(int argc, char *argv[]) {
    for (int i = 0; i < g_i_longueurPhrase; i++) {
        if (g_tc_phrase[i] == g_c_token) {
            g_i_nombreOccurrence++;
        }
    }

    printf("Le nombre d'occurrences de '%c' dans la phrase est : %d\n", g_c_token, g_i_nombreOccurrence);

    return 0;
}