.orig x3000
FUNC_SORT 
    LEA R6, maxstack      

    ; Pousser les registres nécessaires sur la pile
    ADD R6, R6, #-1
    STR R0, R6, #0          ; Sauvegarder R0 sur la pile
    ADD R6, R6, #-1
    STR R1, R6, #0          ; Sauvegarder R1 sur la pile
    ADD R6, R6, #-1
    STR R2, R6, #0          ; Sauvegarder R2 sur la pile
    ADD R6, R6, #-1
    STR R3, R6, #0          ; Sauvegarder R3 sur la pile
    ADD R6, R6, #-1
    STR R4, R6, #0          ; Sauvegarder R4 sur la pile
    ADD R6, R6, #-1
    STR R5, R6, #0          ; Sauvegarder R5 sur la pile

    ; Initialiser les registres pour le parcours du tableau
    LEA R1, tableau  
    AND R2, R2, #0        

    LD R3, taille_tableau
    NOT R3, R3
    ADD R3, R3, #1         

BOUCLE_I
    ADD R4, R2, R3       
    BRzp FIN           

    ADD R5, R2, #0

BOUCLE_J
    BRz  PROCHAIN_I     

    ; Charger les adresses des éléments j et j-1
    ADD R6, R1, R5         
    ADD R7, R5, #-1
    ADD R6, R1, R7      

    ; Charger les valeurs de TABLEAU[j] et TABLEAU[j-1]
    LDR R0, R6, #0     
    LDR R1, R6, #-1       

    ; Pousser les arguments de FUNC_MAX sur la pile
    ADD R6, R6, #-1
    STR R0, R6, #0
    ADD R6, R6, #-1
    STR R1, R6, #0

    JSR FUNC_MAX      

    ; Récupérer le résultat de FUNC_MAX
    LDR R3, R6, #0
    ADD R6, R6, #1          ; Libérer l'espace de la pile

    BRzp PAS_D_ECHANGE      ; Si TABLEAU[j-1] <= TABLEAU[j], ne pas échanger

    ADD R5, R5, #-1         ; Décrémenter j
    BRnzp BOUCLE_J        

PAS_D_ECHANGE
    ADD R5, R5, #-1         ; Décrémenter j
    BRnzp BOUCLE_J        

PROCHAIN_I
    ADD R2, R2, #1          ; Incrémenter i
    BRnzp BOUCLE_I        

FIN
    ; Restaurer les registres sauvegardés de la pile
    ADD R6, R6, #1
    LDR R5, R6, #0          ; Restaurer R5
    ADD R6, R6, #1
    LDR R4, R6, #0          ; Restaurer R4
    ADD R6, R6, #1
    LDR R3, R6, #0          ; Restaurer R3
    ADD R6, R6, #1
    LDR R2, R6, #0          ; Restaurer R2
    ADD R6, R6, #1
    LDR R1, R6, #0          ; Restaurer R1
    ADD R6, R6, #1
    LDR R0, R6, #0          ; Restaurer R0
    ADD R6, R6, #1

    HALT               
FUNC_MAX 
    ; Pousser les registres nécessaires sur la pile
    ADD R6, R6, #-1
    STR R0, R6, #0       
    ADD R6, R6, #-1
    STR R1, R6, #0         

    ; Récupérer les arguments de la pile
    LDR R0, R6, #2        
    LDR R1, R6, #1      

    ; Comparer les deux valeurs
    NOT R1, R1
    ADD R1, R1, #1
    ADD R0, R0, R1        

    ; Stocker le résultat
    BRzp PAS_INFERIEUR
    ADD R0, R0, #1   

PAS_INFERIEUR
    STR R0, R6, #0         

    ; Restaurer les registres de la pile
    ADD R6, R6, #1
    LDR R1, R6, #0       
    ADD R6, R6, #1
    LDR R0, R6, #0       

    RET                  

; Espace mémoire pour les variables
taille_tableau .FILL #10   
tableau
.FILL #6               
.FILL #3
.FILL #7
.FILL #2
.FILL #8
.FILL #3
.FILL #9
.FILL #23
.FILL #2
.FILL #13

maxstack .BLKW #199   
stackstart .FILL #0     

.END
