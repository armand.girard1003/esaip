function jeuDuNbMagique(){
    const NBMAX = prompt("Entrez un nombre maximum");
    const NBCOUPS = prompt("Entrez le nombre de coups maximum");
    const NBMAGIQUE = Math.floor(Math.random() * NBMAX);
    for(let i=0;i<NBCOUPS;i++){
        let nbCoupsRestants=NBCOUPS-(i+1);
        let choix = prompt("Entrez un essai");
        if(choix<NBMAGIQUE){
            alert("Votre choix est inférieur au nombre magique !\nIl vous reste " + nbCoupsRestants + " essais.");
        }
        else if(choix>NBMAGIQUE){
            alert("Votre choix est supérieur au nombre magique !\nIl vous reste " + nbCoupsRestants + " essais.");
        }
        else if(choix=NBMAGIQUE){
            alert("Vous avez trouver le nombre Magique !");
            break;
        }
    }
}

function calculDAB(montant, devise){
    let coupures = new Array();
    let deviseChoisie = '';
    if(devise == 'dollars'){
        coupures = [100, 50, 20, 10, 5, 2, 1, 0.5, 0.25, 0.1, 0.05, 0.01];
        deviseChoisie = " dollars ";
    }
    else if(devise == 'euros'){
        coupures = [500, 200, 100, 50, 20, 10, 5, 2, 1, 0.5, 0.2, 0.1, 0.05, 0.02, 0.01];
        deviseChoisie = " € ";
    }

    let resultat = montant + deviseChoisie + 'représente \n';

    for (let i = 0; i < coupures.length; i++) {
        const nbrBillet = Math.floor(montant / coupures[i]);
        if (nbrBillet > 0) {
            montant = (montant - (nbrBillet * coupures[i])).toFixed(2)
            resultat += nbrBillet + ' x ' + coupures[i] + deviseChoisie + '\n';
        }
    }

    alert(resultat);
}

function DABComplet(){
    const choix = prompt("Etes vous en dollars ou euros ? ('dollars' ou 'euros'");
    const montant = parseFloat(prompt('Entrer votre montant', ''));
    calculDAB(montant, choix);
}

DABComplet();