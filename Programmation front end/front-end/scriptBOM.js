let handle;
function action() {
    if (action.clicked) {
        if (handle) {
            handle.close();
            handle = null;
        }
    } else {
        handle = window.open("http://www.cacaboudin.fr/", "_blank");
        action.clicked = true;
        document.getElementById("test").innerText = "Généreux";
    }
}

document.getElementById('btnSwitch').addEventListener('click',
        function () {
            let p1 = document.getElementById('p1');
            if (p1.innerHTML == 'Bonjour') {
                p1.innerHTML = 'Au revoir';
            } else {
                p1.innerHTML = 'Bonjour';
            }
        }
    );