-- Nom des serveurs qui ont servi un jus de fraises
SELECT DISTINCT s.NomServeur
FROM Serveur s
JOIN Planning p ON s.NuméroServeur = p.NuméroServeur
JOIN Commande c ON p.NuméroTable = c.NuméroTable
JOIN LigneCommande lc ON c.NuméroCommande = lc.NuméroCommande
JOIN Consommation co ON lc.NuméroConsommation = co.NuméroConsommation
WHERE co.NomConsommation = 'Jus de fraises';

--Nombre total de commandes
SELECT COUNT(*) FROM Commande;

--Nombre de cafés bus le 25 décembre 2003
SELECT COUNT(*)
FROM Commande c
JOIN LigneCommande lc ON c.NuméroCommande = lc.NuméroCommande
JOIN Consommation co ON lc.NuméroConsommation = co.NuméroConsommation
WHERE co.NomConsommation = 'Café' AND c.DateCommande = '2003-12-25';

--Chiffre d'affaire par produit vendu (par type de consommation vendue)
SELECT co.NomConsommation, SUM(co.PrixConsommation * lc.Quantité)
FROM Consommation co
JOIN LigneCommande lc ON co.NuméroConsommation = lc.NuméroConsommation
GROUP BY co.NomConsommation;

--Nom du serveur ayant le plus vendu de '7up'
SELECT s.NomServeur
FROM Serveur s
JOIN Planning p ON s.NuméroServeur = p.NuméroServeur
JOIN Commande c ON p.NuméroTable = c.NuméroTable
JOIN LigneCommande lc ON c.NuméroCommande = lc.NuméroCommande
JOIN Consommation co ON lc.NuméroConsommation = co.NuméroConsommation
WHERE co.NomConsommation = '7up'
GROUP BY s.NomServeur
ORDER BY SUM(lc.Quantité) DESC
LIMIT 1;

--Produit(s) le(s) plus cher(s)
SELECT NomConsommation FROM Consommation
WHERE PrixConsommation = (SELECT MAX(PrixConsommation) FROM Consommation);

--Tables la plus performante (en terme de chiffre d'affaire)
SELECT NuméroTable
FROM Commande
GROUP BY NuméroTable
ORDER BY SUM(PrixTotal) DESC
LIMIT 1;

-- Calculer le nouveau prix total pour la commande 1001
UPDATE Commande c
SET c.PrixTotal = (
    SELECT SUM(co.PrixConsommation * lc.Quantité)
    FROM LigneCommande lc
    JOIN Consommation co ON lc.NuméroConsommation = co.NuméroConsommation
    WHERE lc.NuméroCommande = 1001
)
WHERE c.NuméroCommande = 1001;