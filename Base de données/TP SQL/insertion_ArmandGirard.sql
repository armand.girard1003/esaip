-- Supprimer les données de toutes les tables
DELETE FROM LigneCommande;
DELETE FROM Commande;
DELETE FROM Planning;
DELETE FROM Consommation;
DELETE FROM Serveur;
DELETE FROM Tables;

-- Insérer ensuite les nouvelles données comme précédemment
INSERT INTO Serveur (NuméroServeur, NomServeur) VALUES
(1, 'Alice'),
(2, 'Bob');

INSERT INTO Tables (NuméroTable) VALUES
(101),
(102);

INSERT INTO Planning (Id_Planning, NuméroServeur, NuméroTable, DateAffectation) VALUES
(1, 1, 101, '2023-12-20'),
(2, 2, 102, '2023-12-20');

INSERT INTO Commande (NuméroCommande, DateCommande, PrixTotal, NuméroTable) VALUES
(1001, '2023-12-20', 15.50, 101),
(1002, '2023-12-20', 20.75, 102);

INSERT INTO Consommation (NuméroConsommation, NomConsommation, PrixConsommation) VALUES
(5001, 'Café', 2.50),
(5002, 'Croissant', 1.75),
(5003, 'Jus de fraises', 3.00),
(5004, '7up', 2.00);

INSERT INTO LigneCommande (NuméroCommande, NuméroConsommation, Quantité) VALUES
(1001, 5001, 2),  -- 2 Cafés
(1001, 5003, 1),  -- 1 Jus de fraises
(1002, 5004, 3),  -- 3 7up
(1002, 5002, 2);  -- 2 Croissants

-- Insérer une nouvelle commande le 25 décembre 2003
INSERT INTO Commande (NuméroCommande, DateCommande, PrixTotal, NuméroTable)
VALUES
(1003, '2003-12-25', 18.00, 101); -- Ajouter une nouvelle commande pour la table 101 le 25 décembre 2003

-- Insérer des lignes de commande pour cette nouvelle commande
INSERT INTO LigneCommande (NuméroCommande, NuméroConsommation, Quantité)
VALUES
(1003, 5001, 1),  -- 1 Café
(1003, 5002, 2);  -- 2 Croissants
