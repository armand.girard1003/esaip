drop table if exists LigneCommande;
drop table if exists Consommation;
drop table if exists Commande;
drop table if exists Planning;
drop table if exists Tables;
drop table if exists Serveur;

CREATE TABLE Serveur (
    NuméroServeur INT PRIMARY KEY,
    NomServeur VARCHAR(255)
);

CREATE TABLE Tables (
    NuméroTable INT PRIMARY KEY
);

CREATE TABLE Planning (
    Id_Planning INT PRIMARY KEY,
    NuméroServeur INT,
    NuméroTable INT,
    DateAffectation DATE,
    FOREIGN KEY (NuméroServeur) REFERENCES Serveur(NuméroServeur),
    FOREIGN KEY (NuméroTable) REFERENCES Tables(NuméroTable)
);

CREATE TABLE Commande (
    NuméroCommande INT PRIMARY KEY,
    DateCommande DATE,
    PrixTotal DECIMAL,
    NuméroTable INT,
    FOREIGN KEY (NuméroTable) REFERENCES Tables(NuméroTable)
);

CREATE TABLE Consommation (
    NuméroConsommation INT PRIMARY KEY,
    NomConsommation VARCHAR(255),
    PrixConsommation DECIMAL
);

CREATE TABLE LigneCommande (
    NuméroCommande INT,
    NuméroConsommation INT,
    Quantité INT,
    PRIMARY KEY (NuméroCommande, NuméroConsommation),
    FOREIGN KEY (NuméroCommande) REFERENCES Commande(NuméroCommande),
    FOREIGN KEY (NuméroConsommation) REFERENCES Consommation(NuméroConsommation)
);
